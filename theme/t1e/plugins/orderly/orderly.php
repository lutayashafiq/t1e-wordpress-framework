<?php

/*
Plugin Name: Orderly
Description: Orderly adds a sub-menu to a post type that allows you to order the post type manually via drag and drop. This is a standalone plugin that was migrated into the Typeoneerror WordPress Framework which has slightly different features and loads differently. Please check out the Author URI below to utilize the standalone version.
Version: 1.0
Author: Benjamin Borowski
Author URI: https://github.com/typeoneerror/Orderly
License: MIT
*/

/*
The MIT License (MIT)

Copyright (c) 2014 Typeoneerror Studios LLC

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


require_once APP_LIBRARY . '/core/registry.php';


if (!defined('ORDERLY_PATH'))                define('ORDERLY_PATH', APP_FRAMEWORK_URL . '/plugins/orderly');
if (!defined('ORDERLY_DEFAULT_CAPABILITY'))  define('ORDERLY_DEFAULT_CAPABILITY', 'publish_pages');
if (!defined('ORDERLY_DOMAIN'))              define('ORDERLY_DOMAIN', 'com.typeoneerror.wordpress.orderly');
if (!defined('ORDERLY_LIBRARY'))             define('ORDERLY_LIBRARY', dirname(__FILE__) . "/library");
if (!defined('ORDERLY_OPTION_NAME'))         define('ORDERLY_OPTION_NAME', 'orderly_sortable_post_types');
if (!defined('ORDERLY_SCRIPT'))              define('ORDERLY_SCRIPT', 'orderly_script');
if (!defined('ORDERLY_STYLE'))               define('ORDERLY_STYLE', 'orderly_style');


// Store the menus in a global
// Why not? It’s WordPress
// TODO: convert to Registry or contained plugin
$__orderly_menus = array();


// include specific functionality based on the context
if (is_admin())
{
    include_once ORDERLY_LIBRARY . "/orderly-admin.php";
}


/**
 * Runs when Orderly initializes after WP is loaded.
 *
 * Adds any currently set sortables to the sortable stack.
 *
 * @return void
 */
function orderly_wp_loaded()
{
    global $__orderly_menus;

    $option = get_option(ORDERLY_OPTION_NAME);
    $options = explode(',', $option);

    foreach($options as $post_type)
    {
        if (post_type_exists($post_type) && !isset($__orderly_menus[$post_type]))
        {
            orderly_register_orderable_post_type($post_type);
        }
    }
}
add_action('wp_loaded', 'orderly_wp_loaded');


/**
 * Register a post type as an orderable post type.
 *
 * @param string $post_type  Name of the post type registered with register_post_type.
 * @param array $options     Additional options as follows:
 *
 *     string capability:  Role required to order posts (http://codex.wordpress.org/Roles_and_Capabilities)
 *
 * @return void
 */
function orderly_register_orderable_post_type($post_type, $options = array())
{
    global $__orderly_menus;

    $defaults = array(
        'capability' => ORDERLY_DEFAULT_CAPABILITY, // default capability is Editor
    );
    $options = array_merge($defaults, $options);

    $__orderly_menus[$post_type] = $options;
}


/**
 * Register multiple post types as orderable post types.
 * @param array $post_types  Names of the post types registered with register_post_type.
 * @param array $options     Additional options as follows:
 *
 *     string capability:  Role required to order posts (http://codex.wordpress.org/Roles_and_Capabilities)
 *
 * @return void
 */
function orderly_register_orderable_post_types(array $post_types, $options = array())
{
    foreach ($post_types as $post_type)
    {
        orderly_register_orderable_post_type($post_type, $options);
    }
}


/**
 * Unregister a post type previously registered with orderly_register_orderable_post_type.
 *
 * @param string $post_type  Name of custom post type.
 * @return void
 */
function orderly_unregister_orderable_post_type($post_type)
{
    global $__orderly_menus;

    if (isset($__orderly_menus[$post_type]))
    {
        unset($__orderly_menus[$post_type]);
    }
}


/**
 * Unregister all orderable types.
 *
 * @return void
 */
function orderly_unregister_all()
{
    global $__orderly_menus;

    $__orderly_menus = array();
}


/**
 * @return array  Currently registered post types.
 */
function orderly_get_registered_post_types()
{
    global $__orderly_menus;
    return $__orderly_menus;
}


/**
 * Helper to clear caches when order changes.
 */
function orderly_clear_cache()
{
    if (function_exists('w3tc_pgcache_flush'))
    {
        w3tc_pgcache_flush();
    }
    elseif (function_exists('wp_cache_clear_cache'))
    {
        wp_cache_clear_cache();
    }
}
