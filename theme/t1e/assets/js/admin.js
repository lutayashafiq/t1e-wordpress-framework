/*

admin.js

Adds custom javascript to WordPress admin

*/

jQuery(document).ready(function($)
{
  var date_format = 'yy-mm-dd';

  $('.t1e-datepicker').each(function(index, element)
  {
    var alt_field_id = '#' + $(this).attr('data-alt-field')
      , $alt_field = $(alt_field_id)
      , default_date = $alt_field.val();

    if (!default_date.length)
    {
      default_date = new Date();
    }

    $('.t1e-datepicker-inline', $(this)).datepicker({
        defaultDate: default_date
      , dateFormat: date_format
      , altField: alt_field_id
      , altFormat: date_format
      , showButtonPanel: true
    });

  });

  // TODO: move to own file

  if ($('#t1e-relationships').length)
  {
    var $rel_tabs = $('#t1e-relationships-internal-tabs li');

    $('a', $rel_tabs).click(function()
    {
      var $tab = $(this).parent()
        , $tabs = $('li', $tab.parent())
        , tab_index = $tabs.index($tab)
        , tab_url = $(this).attr('href')

      $(this).parent().addClass('tabs').siblings('li').removeClass('tabs');
      $('.tabs-panel').hide();

      $(tab_url).show();
      setUserSetting('t1e_rp_ti', tab_index);
      return false;
    });

    if (getUserSetting('t1e_rp_ti'))
    {
      var selectedIndex = parseInt(getUserSetting('t1e_rp_ti'))
        , $selectedTab = $rel_tabs.eq(selectedIndex);
      $('a', $selectedTab).click();
    }
  }

});
