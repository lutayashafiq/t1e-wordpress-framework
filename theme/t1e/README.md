T1E WordPress Framework
=======================

Overview
--------

The T1E (aka Typeoneerror) WordPress Framework is a (mostly) object-oriented framework built on top of WordPress. It's essentially a theme framework focused on making custom CMSes with WordPress much simpler and less wordy.
