<?php

/**
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\admin;


{
    /**
     * @link \t1e\core\Plugin
     */
    require_once APP_LIBRARY . '/core/plugin.php';
}


if (!class_exists(__NAMESPACE__ . '\Translations')):

/**
 * Loads a translations.ini file from your theme’s root directory
 *
 * Can be used to translate parts of the WordPress admin
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Translations extends \t1e\core\Plugin
{
    /**
     * Apply filters on WordPress hooks
     *
     * @var array
     */
    protected $_filters = array(
        'gettext' => array(null, 3),
    );

    /**
     * Stored translation data
     *
     * @var array
     */
    protected $_translations;

    /**
     * Parse translation ini file at setup time
     *
     * @return void
     */
    protected function _setup()
    {
        $this->_parseTranslationsFile();
    }

    /**
     * Parse translation ini file in theme directory
     *
     * @return void
     */
    protected function _parseTranslationsFile()
    {
        $file = get_stylesheet_directory() . '/translations.ini';
        if (file_exists($file))
        {
            $this->_translations = parse_ini_file($file);
        }
    }

    /**
     * When WordPress requests a text, see if there are any translation overrides
     *
     * TODO: is there a better WordPress way of loading translations?
     *
     * @param string $translations  Translated text
     * @param string $text          Text to translate
     * @param string $domain        Text domain, unique identifier for retrieving translated strings
     *
     * @return string               Translated text
     */
    public function gettext($translation, $text, $domain)
    {
        if (isset($this->_translations[$text]))
        {
            return $this->_translations[$text];
        }
        return $translation;
    }

    /**
     * Returns the parsed ini translations
     *
     * @return array  Translations array
     */
    public function get_translations()
    {
        return $this->_translations;
    }
}

endif;
