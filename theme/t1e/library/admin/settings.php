<?php

/**
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\admin;

use \t1e\core\FieldTypes as FieldTypes;
use \t1e\core\Inflector as Inflector;


{
    /**
     * @link \t1e\admin\Page
     */
    require_once APP_LIBRARY . '/admin/page.php';
}


if (!class_exists(__NAMESPACE__ . '\Settings')):

/**
 * Adds a settings page to the Appearance menu. Creates general forms
 * for managing global settings for the theme/website.
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Settings extends Page
{
    /**
     * Suffix for settings group name
     *
     * @var string
     */
    const SETTINGS_GROUP = '_settings_group';

    /**
     * Suffix for settings post data name
     *
     * @var string
     */
    const SETTINGS_VALUES = '_settings';

    /**
     * Name of settings file to look for in theme directory
     *
     * @var string
     */
    const SETTINGS_FILE = 'settings.json';

    /**
     * Default setting fields parsed from json file
     *
     * @var array
     */
    protected $_default_settings = array();

    /**
     * Same as $_default_settings but just key => value array
     *
     * @var array
     */
    protected $_default_settings_keyed = array();

    /**
     * Path to settings file
     *
     * @var string
     */
    protected $_settings_file = null;

    /**
     * Raw parased json data
     *
     * @var object
     */
    protected $_raw_data = null;

    /**
     * Setup the plugin and set path to an expected settings file
     *
     * @return void
     */
    protected function _setup()
    {
        parent::_setup();

        $this->_settings_file = APP_THEME_PATH . '/' . self::SETTINGS_FILE;

        if (file_exists($this->_settings_file))
        {
            $this->_raw_data = $this->_parse_settings_file();
        }
        else
        {
            echo sprintf(__('Create %s to use global settings', APP_LANG), $this->_settings_file);
        }
    }


    /**
     * Override method to add pages to sidebar menu
     *
     * @return void
     */
    public function admin_menu()
    {
        $title = 'Website Settings';
        add_theme_page(
            $title,
            $title,
            'edit_theme_options',
            'website-settings',
            array($this, 'website_settings')
        );
    }

    /**
     * Admin init hook.
     *
     * Registers a settings group with WordPress for this theme.
     *
     * @return void
     */
    public function admin_init()
    {
        register_setting(
            APP_LANG . self::SETTINGS_GROUP,
            APP_LANG . self::SETTINGS_VALUES,
            array($this, 'website_settings_validation')
        );
    }

    /**
     * Returns saved settings or defaults
     *
     * @return array
     */
    public function get_settings()
    {
        $settings = get_option(APP_LANG . self::SETTINGS_VALUES, $this->_default_settings_keyed);
        return $settings;
    }

    /**
     * Renders the main settings page
     *
     * @return void
     */
    public function website_settings()
    {
        // Check that the user is allowed to update options
        if (!current_user_can('edit_theme_options'))
        {
            wp_die(__('You do not have sufficient permissions to access this page.', APP_LANG));
        }

        if ($this->_raw_data === false)
        {
            wp_die(sprintf(__('Could not parse settings from <em>%s</em>', APP_LANG), $this->_settings_file));
        }

        $settings = $this->get_settings();

        if (!empty($settings))
        {
            $render_data = array(
                'raw_data'               => $this->_raw_data,
                'default_settings'       => $this->_default_settings,
                'default_settings_keyed' => $this->_default_settings_keyed,
                'settings'               => $settings,
                'option_group'           => APP_LANG . self::SETTINGS_GROUP,
                'values_group'           => APP_LANG . self::SETTINGS_VALUES,
            );
            $this->_render('settings', $render_data);
        }
        else
        {
            echo sprintf(__('Could not create settings page from %s', APP_LANG), $this->_settings_file);
        }
    }

    /**
     * Validates settings
     *
     * @return void
     */
    public function website_settings_validation($input)
    {
        $settings = $this->get_settings();

        // echo "pre sanitize"; pr($input);

        foreach ($input as $key => $value)
        {
            if (isset($this->_default_settings[$key]))
            {
                $type = $this->_default_settings[$key]['type'];
                switch ($type)
                {
                    case FieldTypes::TEXTAREA:
                        $input[$key] = wp_filter_nohtml_kses($input[$key]);
                        break;

                    default:
                        $input[$key] = wp_filter_post_kses($input[$key]);
                        break;
                }
            }
            else
            {
                unset($input[$key]);
            }
        }

        // echo "default_settings"; pr($this->_default_settings); echo "current"; pr($settings); echo "new"; pr($input); exit;

        return $input;
    }

    /**
     * Parses a settings.json file in the theme folder and preps the data
     * for presentation in the settings.php admin view.
     *
     * @return array|false
     */
    protected function _parse_settings_file()
    {
        $settings_content = file_get_contents($this->_settings_file);
        $raw_data = json_decode($settings_content);
        if (!$raw_data)
        {
            return false;
        }

        $default_settings = array();
        $default_settings_keyed = array();

        foreach ($raw_data as $key => $value)
        {
            $setting_key = $key;
            $setting_value = $value;
            $setting_type = FieldTypes::TEXT;
            $setting_label = Inflector::humanize($key);
            $setting_placeholder = $setting_label;
            $setting_option_label = $setting_label;
            $setting_desc = null;
            $setting_values = array();

            if (is_object($value))
            {
                $setting_value = (isset($value->value)) ? $value->value : '';
                if (isset($value->type)) $setting_type = $value->type;
                if (isset($value->label)) $setting_label = $value->label;
                if (isset($value->placeholder)) $setting_placeholder = $value->placeholder;
                if (isset($value->description)) $setting_desc = $value->description;
                if (isset($value->option_label)) $setting_option_label = $value->option_label;
                if (isset($value->values)) $setting_values = (array)$value->values;
            }

            $default_settings[$key] = array(
                'value'         => $setting_value,
                'type'          => $setting_type,
                'label'         => $setting_label,
                'option_label'  => $setting_option_label,
                'placeholder'   => $setting_placeholder,
                'description'   => $setting_desc,
                'values'        => $setting_values,
            );

            $default_settings_keyed[$key] = $setting_value;
        }

        $this->_default_settings = $default_settings;
        $this->_default_settings_keyed = $default_settings_keyed;

        return $raw_data;
    }
}

endif;
