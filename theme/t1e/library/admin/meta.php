<?php

/**
 * @package    \t1e\admin
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\admin;

use \t1e\core\FieldTypes as FieldTypes;
use \t1e\core\Inflector as Inflector;
use \t1e\core\Registry as Registry;


{
    /**
     * @see ../core/helpers.php
     */
    require_once APP_LIBRARY . '/core/helpers.php';

    /**
     * @link \t1e\core\Plugin
     */
    require_once APP_LIBRARY . '/core/plugin.php';

    /**
     * @link \t1e\core\Registry
     */
    require_once APP_LIBRARY . '/core/registry.php';

    /**
     * @link \t1e\admin\MetaField
     */
    require_once APP_LIBRARY . '/admin/meta-field.php';

    /**
     * @link \t1e\admin\Related
     */
    require_once APP_LIBRARY . '/admin/related.php';
}


if (!class_exists(__NAMESPACE__ . '\MetaBox')):

/**
 * Defines a structure for an WordPress admin meta box
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class MetaBox
{
    /**
     * Custom post type
     *
     * @var string
     */
    public $post_type;

    /**
     * Fields to add to MetaBox
     *
     * @var array
     */
    public $fields = array();

    /**
     * Title of MetaBox
     *
     * @var string
     */
    public $title;

    /**
     * Create a MetaBox structure
     *
     * @param string $post_type  Name of custom post type to add meta box
     * @param array $fields      An array of fields to add to meta box, e.g.:
     *
     *     array(
     *         'company',
     *         'option_name' => \t1e\core\FieldTypes::OPTION,
     *         'url' => array(
     *             'placeholder' => 'Company URL',
     *          ),
     *     )
     *
     * @param  string $title     Title of meta box
     */
    public function __construct($post_type, array $fields, $title = null)
    {
        $this->post_type = $post_type;
        $this->fields = $fields;
        $this->title = $title;
    }
}

endif;


if (!class_exists(__NAMESPACE__ . '\Meta')):

/**
 * Class for working with meta data in the WordPress admin
 *
 * Performs operations such as adding custom meta boxes and
 * auto-magically adding sortable columns to list layouts in
 * the WordPress admin interface
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Meta extends \t1e\core\Plugin
{
    /**
     * List of WordPress hooks to add at run time
     *
     * @var array
     * @see \t1e\core\Plugin::$_actions
     */
    protected $_actions = array(
        'admin_init',
        'add_meta_boxes' => array(10, 3),
    );

    /**
     * MetaBox instances added to the current post type editor
     *
     * Boxes are added when you use `\t1e\App::add_module()` and
     * stored here so they can all be added during the WordPress
     * `admin_init` action hook
     *
     * @var array
     */
    protected $_meta_boxes = array();

    /**
     * Instance of \t1e\admin\Related class
     *
     * Adds a meta box to the edit page that allows you to specific
     * relationships to another post type from the post type you
     * are currently editing
     *
     * @var Related
     */
    protected $_related = null;

    /**
     * When admin_init WordPress action is called, add any
     * pending meta boxes to the post type
     *
     * @return void
     */
    public function admin_init()
    {
        if (!empty($this->_meta_boxes))
        {
            foreach ($this->_meta_boxes as $meta_box)
            {
                $this->add_meta_box($meta_box);
            }
        }
    }

    /**
     * Adds meta boxes to the edit interface
     *
     * In this case, we add the Related class which creates
     * a box for custom post type relationships
     *
     * @param string $post_type  The post type we are editing
     * @param WP_Post $post      WordPress post object if updating
     *
     * @return void
     */
    public function add_meta_boxes($post_type, $post)
    {
        $this->_related = new Related($post_type, $post);
    }

    /**
     * Updates a meta data item on a post by post id
     *
     * @param int $post_id  The id of the post being edited
     * @param string $key   The key of the metadata being edited
     *
     * @return void
     */
    public function update_post_meta_by_key($post_id, $key)
    {
        if (!empty($_POST[$key]))
        {
            update_post_meta($post_id, $key, $_POST[$key]);
        }
        else
        {
            delete_post_meta($post_id, $key);
        }
    }

    /**
     * Prepares a WordPress meta box to page when editing a post type item
     *
     * This version acts like a "lazy load" function and creates a MetaBox
     * instance in preparation for when `admin_init` is called
     *
     * @param string $post_type  Name of custom post type to add meta box
     * @param array $fields      An array of fields to add to meta box, e.g.:
     *
     *     array(
     *         'company',
     *         'option_name' => \t1e\core\FieldTypes::OPTION,
     *         'url' => array(
     *             'placeholder' => 'Company URL',
     *          ),
     *     )
     *
     * @param  string $title     Title of meta box
     *
     * @return void
     */
    public function prepare_meta_box_for_type($post_type, array $fields, $title = null)
    {
        $this->_meta_boxes[] = new MetaBox($post_type, $fields, $title);
    }

    /**
     * Adds a WordPress meta box to page when editing a post type item
     *
     * @param string $post_type  Name of custom post type to add meta box
     * @param array $fields      An array of fields to add to meta box, e.g.:
     *
     *     array(
     *         'company',
     *         'option_name' => \t1e\core\FieldTypes::OPTION,
     *         'url' => array(
     *             'placeholder' => 'Company URL',
     *          ),
     *     )
     *
     * @param  string $title     Title of meta box
     *
     * @return void
     */
    public function add_meta_box_for_type($post_type, array $fields, $title = null)
    {
        $meta_box = new MetaBox($post_type, $fields, $title);
        $this->add_meta_box($meta_box);
    }

    /**
     * Create a meta box given an instance of the meta box structure
     *
     * @param string $meta_box  Meta box instance to add to admin
     *
     * @return void
     */
    public function add_meta_box(MetaBox $meta_box)
    {
        $post_type = $meta_box->post_type;
        $fields = $meta_box->fields;
        $title = $meta_box->title;

        // meta box title
        if (is_null($title)) { $title = ucfirst(Inflector::singularize($post_type)) . __(' Options', APP_LANG); }

        // register fields with the registry
        $field_keys = $this->_append_post_type_to_fields(array_keys_mixed($fields), $post_type);
        // echo "<b>$post_type</b>"; pr($field_keys);

        $registry = Registry::get_instance();
        if (!isset($registry->fields_to_save)) { $registry->fields_to_save = array(); }

        $registry->fields_to_save = array_merge($registry->fields_to_save, $field_keys);
        $registry->modules[$post_type]['fields_to_save'] = $field_keys;
        $registry->modules[$post_type]['fields_raw'] = $fields;

        // add_meta_box($id, $title, $callback, $page, $context, $priority, $callback_args);
        //
        // $context:  The part of the page where the edit screen section should be shown ('normal', 'advanced', or 'side'). Note that 'side' doesn't exist before 2.7.
        // $priority: The priority within the context where the boxes should show ('high', 'core', 'default' or 'low')
        add_meta_box("{$post_type}_meta_box_id",
                     $title,
                     array($this, 'build_meta_box'),
                     $post_type,
                     'normal',
                     'high',
                     array(
                        'fields'     => $fields,
                        'post_type'  => $post_type,
                     )
        );

        $listable_columns = $this->get_list_columns_for_post_type($post_type);
        if (count($listable_columns))
        {
            add_action("manage_{$post_type}_posts_custom_column", array($this, 'custom_columns'), 5, 2);
        }

        $sortable_columns = $this->get_sortable_columns_for_post_type($post_type);
        if (count($sortable_columns))
        {
            add_filter('request', array($this, 'sortable_request'));
            add_filter("manage_edit-{$post_type}_sortable_columns", array($this, 'sortable_columns'));
        }
    }

    /**
     * Render a custom meta box to the edit screen
     *
     * @param WP_Post $post  WordPress post object
     * @param array $args    Meta box arguments
     *
     * @return void
     */
    public function build_meta_box($post, $args)
    {
        $params = $args['args'];

        // bail out if we don't have the requirements
        if (!isset($params['post_type']) || empty($params['fields'])) return;

        $post_type = $params['post_type'];
        $fields = $params['fields'];

        // bail out if post type doesn't exist
        if (!post_type_exists($post_type)) return; ?>

        <div class="t1e-custom-config">

        <?php

        // loop through the fields and render them if a renderer exists for the field type
        foreach ($fields as $key => $value):

            // default field options
            $field_type = FieldTypes::TEXT;
            $field_title = $value;
            $inflect_title = true;
            $default_value = '';
            $placeholder = null;
            $values = isset($value['values']) ? $value['values'] : null;
            $allow_blank = isset($value['allow_blank']) ? (bool)$value['allow_blank'] : true;

            if (is_array($value))
            {
                // if field is represented as an array
                $field_name = "{$post_type}_{$key}";

                if (isset($value['field_type'])) $field_type = $value['field_type'];

                if (isset($value['title'])) $field_title = $value['title'];
                else $field_title = Inflector::humanize($key);

                if (isset($value['placeholder'])) $placeholder = $value['placeholder'];
                if (isset($value['default'])) $default_value = $value['default'];

                $inflect_title = false;
            }
            else if (!is_numeric($key))
            {
                // if field is presented as "field_name" => FieldTypes::OPTION
                $field_type = $value;
                $field_name = "{$post_type}_{$key}";
                $field_title = $key;
            }
            else
            {
                $field_name = "{$post_type}_{$value}";
            }

            if ($inflect_title)
            {
                $field_title = Inflector::humanize($field_title);
            }

            $single = true; // get only single value for meta, not array
            // get value from database
            $current_value = get_post_meta($post->ID, $field_name, $single);
            if (!$current_value)
            {
                $current_value = $default_value;
            }
        ?>

        <div class="t1e-custom-config-element">

        <?php
            $field_type_name = Inflector::camelize($field_type, true, true);
            $field_type_class = "\\t1e\\admin\\meta_field\\{$field_type_name}";
            if (class_exists($field_type_class))
            {
                $field_type_options = array(
                    'title'         => $field_title,
                    'placeholder'   => $placeholder,
                    'inflect_title' => $inflect_title,
                    'current_value' => $current_value,
                    'values'        => $values,
                    'allow_blank'   => $allow_blank,
                );
                $field_type_instance = new $field_type_class($field_name, $field_type_options);
                $field_type_instance->render();
            }
            else
            {
                throw new \t1e\core\Error(sprintf('Field type %s does not exist', $field_type_name));
            }
        ?>

        </div><!--.t1e-custom-config-element-->

    <?php

        endforeach;

    ?>

    </div><!--.t1e-custom-config-->

    <?php

    } // build_meta_box

    /**
     * Display the content for custom columns
     *
     * This is automatically added as an action when you add a meta box to a post type
     *
     * @param string $column_name  Name of column being rendered
     * @param int    $id           ID of item being rendered
     *
     * @return void
     */
    public function custom_columns($column_name, $id)
    {
        global $post;

        $columns = $this->get_list_columns_for_post_type($post->post_type);

        if (array_key_exists($column_name, $columns))
        {
            $column = $columns[$column_name];
            $value = \t1e\get_meta_data($column_name);

            if (!is_null($column['callback']) && is_callable($column['callback']))
            {
                $func = $column['callback'];
                echo $func($value);
            }
            else if (stripos($column_name, '_url') !== false)
            {
                // url
                echo "<a href=\"" . esc_url($value) . "\" target=\"_blank\">" . $value . "</a>";
            }
            else if (stripos($column_name, 'is_') !== false || stripos($column_name, 'has_') !== false)
            {
                // it’s a checkbox
                $icon = intval($value) ? 'yes' : 'no-alt';
                echo '<span class="dashicons-before dashicons-' . $icon . '">&nbsp;</span>';
            }
            else
            {
                echo $value;
            }
        }
    }

    /**
     * Returns sortable columns for WordPress listings
     *
     * @param array $columns  List of columns that are sortable
     *
     * @return array          Full list of sortable columns after filter
     */
    public function sortable_columns($columns)
    {
        global $post;

        $sortable_columns = $this->get_sortable_columns_for_post_type($post->post_type);
        $new_columns = array_merge($columns, $sortable_columns);

        return $new_columns;
    }

    /**
     * Sort on custom columns by overriding the request vars
     *
     * If the requested sort column exists as a custom field on
     * the post type, the orderby is updated to sort on meta_value
     *
     * @param string[] $query_vars  Query vars requested
     *
     * @return string[]             Updated query vars based on custom sortable columns
     */
    public function sortable_request($query_vars)
    {
        if (isset($query_vars['orderby']))
        {
            $orderby = $query_vars['orderby'];
            $post_type = $query_vars['post_type'];

            $sortable_columns = $this->get_sortable_columns_for_post_type($query_vars['post_type']);
            if (array_key_exists($orderby, $sortable_columns))
            {
                $query_vars['meta_key'] = $this->_append_post_type_to_field($orderby, $post_type);
                $query_vars['orderby'] = 'meta_value';
            }
        }

        // pr($query_vars);

        return $query_vars;
    }

    /**
     * Gets list of columns to show in a listing for `$post_type`
     *
     * @param string $post_type  Post type to get columns for
     *
     * @return string[]          List of listable columns
     */
    public function get_list_columns_for_post_type($post_type)
    {
        $registry = Registry::get_instance();
        $fields = array();

        if ($registry->modules[$post_type])
        {
            $module = $registry->modules[$post_type];
            if (isset($module['fields_raw']))
            {
                $fields_raw = $module['fields_raw'];
                foreach ($fields_raw as $field_name => $field)
                {
                    $show_in_list = isset($field['show_in_list']) ? intval($field['show_in_list']) : false;
                    if ($show_in_list)
                    {
                        $field_title = Inflector::humanize($field_name);

                        if (isset($field['list_title'])) $field_title = $field['list_title'];
                        elseif (isset($field['title'])) $field_title = $field['title'];

                        $fields[$field_name] = array(
                            'title'    => __($field_title, APP_LANG),
                            'callback' => isset($field['list_value']) ? $field['list_value'] : null,
                        );
                    }
                }
            }
        }

        return $fields;
    }

    /**
     * Gets list of sortable columns for listing `$post_type`
     *
     * @param string $post_type  Post type for which to get sortable columns
     *
     * @return string[]          List of sortable columns
     */
    public function get_sortable_columns_for_post_type($post_type)
    {
        $registry = Registry::get_instance();
        $fields = array();

        if ($registry->modules[$post_type])
        {
            $module = $registry->modules[$post_type];
            if (isset($module['fields_raw']))
            {
                $fields_raw = $module['fields_raw'];
                foreach ($fields_raw as $field_name => $field)
                {
                    $sortable = isset($field['sortable']) ? intval($field['sortable']) : false;
                    if ($sortable)
                    {
                        $order = null; // TODO: implement ordering desc
                        $fields[$field_name] = $field_name;
                    }
                }
            }
        }

        return $fields;
    }

    /**
     * Appends post type to list of fields
     *
     * @param string[]   List of fields
     * @param string     Post type to append to fields
     *
     * @return string[]  New field names
     */
    protected function _append_post_type_to_fields($fields, $post_type)
    {
        $callback = array($this, '_append_post_type_to_field');
        $filler = array_fill(0, count($fields), $post_type);
        return array_map($callback, $fields, $filler);
    }

    /**
     * Appends post type to a single field
     *
     * @param string $field      Field name
     * @param string $post_type  Post type to append to field
     *
     * @return string  New field name
     */
    protected function _append_post_type_to_field($field, $post_type)
    {
        return $post_type . "_{$field}";
    }
}

endif;


namespace t1e;


if (!function_exists(__NAMESPACE__ . '\get_meta_data')):

/**
 * Get meta-data associated with a post
 *
 * @param string|array $key   Name of metadata keys to get
 * @param bool $single        Get a single item rather than array of metadata for each key
 * @param bool $prefix        Metadata key is stored with post-type prefix if true e.g.
 *
 *     with a post type of "custom", this would look metadata called "custom_test":
 *
 *          get_meta_data('test');
 *
 *     and this would look up metadata called "test":
 *
 *          get_meta_data('test', true, false);
 *
 * @return mixed              An object of metadata by key or a single value
 */
function get_meta_data($key = null, $single = true, $prefix = true)
{
    global $post;

    if (!is_array($key) && !is_null($key))
    {
        if ($prefix)
        {
            $key = "{$post->post_type}_{$key}";
        }
        $data = get_post_meta($post->ID, $key, $single);
        return $data;
    }
    else
    {
        $meta = get_post_meta($post->ID);
        $data = array();

        if (empty($key))
        {
            foreach ($meta as $key => $value)
            {
                // skip metadata that starts with '_'
                if (stripos($key, '_') === 0) continue;
                // leave metadata that is not prefixed with post-type alone
                $new_key = (!$prefix || stripos($key, "{$post->post_type}_") === false) ? $key : substr($key, strlen($post->post_type) + 1);
                $data[$new_key] = maybe_unserialize($single ? $meta[$key][0] : $meta[$key]);
            }
        }
        else
        {
            foreach ($key as $key_item)
            {
                $data_value = null;
                $the_key = $prefix ? "{$post->post_type}_{$key_item}" : $key_item;
                if (isset($meta[$the_key]))
                {
                    $data_value = $single ? $meta[$the_key][0] : $meta[$the_key];
                    $data_value = maybe_unserialize($data_value);
                }
                $data[$key_item] = $data_value;
            }
        }

        return (object)$data;
    }
}

endif;
