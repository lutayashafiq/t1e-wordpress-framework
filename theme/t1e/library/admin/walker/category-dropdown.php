<?php

/**
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\admin\walker;


if (!class_exists(__NAMESPACE__ . '\CategoryDropdown')):

/**
 * Custom walker for adding a dropdown that allows you to
 * filter on custom post types in post listings.
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class CategoryDropdown extends \Walker_CategoryDropdown
{
    function start_el(&$output, $category, $depth = 0, $args = array(), $id = 0)
    {
        $pad = str_repeat('&nbsp;', $depth * 3);

        /** This filter is documented in wp-includes/category-template.php */
        $cat_name = apply_filters('list_cats', $category->name, $category);

        $output .= "\t<option class=\"level-$depth\" value=\"" . $category->slug . "\"";
        if ($category->term_id == $args['selected'])
        {
            $output .= ' selected="selected"';
        }
        $output .= '>';
        $output .= $pad . $cat_name;
        if ($args['show_count'])
        {
            $output .= '&nbsp;&nbsp;(' . number_format_i18n($category->count) . ')';
        }
        $output .= "</option>\n";
    }
}

endif;
