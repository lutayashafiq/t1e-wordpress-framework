<?php

/**
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\admin;

use \t1e\core\Registry as Registry;


{
    /**
     * @link t1e\admin\Page
     */
    require_once APP_LIBRARY . '/admin/page.php';
}


if (!class_exists(__NAMESPACE__ . '\Related')):

/**
 * Related creates a relationship box as a sidebar meta box on the editor page
 *
 * It is instantiated at run-time during the <code>add_meta_boxes</code> WordPress action
 *
 * If a custom post type has defined a relationship to another post
 * this class is instantiated and a select box for related posts is displayed
 *
 * Data is saved as meta-data when the post is saved
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */

class Related extends \t1e\admin\Page
{
    /**
     * An array of relationships as "post_type" => "relationship_type"
     *
     * @var array
     */
    protected $_relationships = array();

    /**
     * WordPress WP_Post object if we are updating a post
     *
     * @var WP_Post
     */
    protected $_post = null;

    /**
     * Post type we are editing
     *
     * @var string
     */
    protected $_post_type = null;

    /**
     * Create a Related instance
     *
     * @param string $post_type  Post type we are editing
     * @param WP_Post $post      WordPress WP_Post object if we are updating a post
     */
    public function __construct($post_type, $post)
    {
        $this->_post_type = $post_type;
        $this->_post = $post;

        parent::__construct();
    }

    /**
     * Create meta boxes on setup if relationships exist
     *
     * @return void
     */
    protected function _setup()
    {
        parent::_setup();

        $registry = Registry::get_instance()->modules;
        $module = isset($registry[$this->_post_type]) ? $registry[$this->_post_type] : null;
        if ($module && isset($module['relationships']))
        {
            $this->_relationships = (array)$module['relationships'];
            if (!empty($this->_relationships))
            {
                $this->_add_meta_box();
            }
        }
    }

    /**
     * Add additional metaboxes when the edit page loads
     *
     * @return void
     */
    protected function _add_meta_box()
    {
        $id = "t1e-relationships";
        $title = __('Relationships', APP_LANG);
        $callback = array($this, 'relationship_meta_boxes');
        $context = 'side'; // ('normal', 'advanced', or 'side')
        $priority = 'default'; // ('high', 'core', 'default', or 'low')
        $callback_args = array();

        add_meta_box($id, $title, $callback, $this->_post_type, $context, $priority, $callback_args);
    }

    /**
     * Callback for adding metaboxes, renders the metabox view
     *
     * @param WP_Post $post   WordPress post we are editing
     * @param array $metabox  Metabox callback args
     *
     * @return void
     */
    public function relationship_meta_boxes($post, $metabox)
    {
        $metadata = \t1e\get_meta_data('relationships', true, false);
        $this->_render('related', array(
            'relationships'    => $this->_relationships,
            'parent_post_type' => $this->_post_type,
            'metadata'         => $metadata,
        ));
    }
}

endif;
