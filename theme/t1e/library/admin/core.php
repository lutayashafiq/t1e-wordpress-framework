<?php

/**
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\admin;

use \t1e\core\Registry as Registry;


{
    /**
     * @link Meta
     */
    require_once APP_LIBRARY . '/admin/meta.php';

    /**
     * @link Settings
     */
    require_once APP_LIBRARY . '/admin/settings.php';

    /**
     * @link Translations
     */
    require_once APP_LIBRARY . '/admin/translations.php';

    /**
     * @link walker\CategoryDropdown
     */
    require_once APP_LIBRARY . '/admin/walker/category-dropdown.php';

    /**
     * @link \t1e\core\Plugin
     */
    require_once APP_LIBRARY . '/core/plugin.php';

    /**
     * @link \t1e\core\Registry
     */
    require_once APP_LIBRARY . '/core/registry.php';
}


if (!class_exists(__NAMESPACE__ . '\Core')):

/**
 * Bootstraps functionality specific to WordPress admin URLs
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Core extends \t1e\core\Plugin
{
    ////////////////////
    // Public objects //
    ////////////////////

    /**
     * @var \t1e\admin\Meta
     */
    public $meta;

    ///////////////////////
    // Protected objects //
    ///////////////////////

    /**
     * @var \t1e\admin\Settings
     */
    protected $_settings;

    /**
     * @var \t1e\admin\Translations
     */
    protected $_translations;

    /////////////
    // Actions //
    /////////////

    protected $_actions = array(
        'admin_init',
        'admin_menu',
        'admin_head',
        'pre_get_posts',
        'save_post' => array(5, 3),
        'manage_posts_columns' => array(5, 2)
    );

    /////////////
    // Filters //
    /////////////

    protected $_filters = array(
        'admin_footer_text'
    );

    ///////////////////////
    // Setup before init //
    ///////////////////////

    /**
     * Setup the admin core
     *
     * @return void
     */
    protected function _setup()
    {
        $this->meta = new Meta();

        // If translations is active and a translations.ini file
        // exists in the theme directory, you can translate parts of
        // the WordPress administration interface.
        if (APP_OPT_USE_TRANSLATIONS)
        {
            $this->_translations = new Translations();
        }

        // Only bootstrap the settings class if the CMS uses settings pages
        if (APP_OPT_USES_SETTINGS)
        {
            $this->_settings = new Settings();
        }

        // remote rich editing if not needed
        if (!APP_OPT_ALLOW_RICHEDIT)
        {
            add_filter('user_can_richedit', create_function('$wp_rich_edit', 'return false;'));
        }

        // If WordPress post thumbnails are active, we add some custom
        // functionality to display them in content lists
        if (APP_OPT_USE_THUMBNAILS)
        {
            $this->_add_filter('admin_post_thumbnail_html', 5, 2);
            $this->_add_action('manage_posts_custom_column', 5, 2);
        }
    }

    ////////////////////
    // Initialization //
    ////////////////////

    /**
     * Called prior to admin_menu
     *
     * @return void
     */
    public function admin_init()
    {
        $customize_url = 'customize.php?return=' . urlencode(wp_unslash($_SERVER['REQUEST_URI']));
        remove_submenu_page('themes.php', $customize_url);
        remove_submenu_page('themes.php', 'theme-editor.php');
    }

    /**
     * Configure the admin menu for the WordPress admin
     *
     * @return void
     */
    public function admin_menu()
    {
        $remove_menus = array();
        $remove_submenus = array();

        // don't need posts
        if (!APP_OPT_USES_POSTS)
        {
            $remove_menus[] = 'edit.php';
        }

        // don't need comments since there are none
        if (!APP_OPT_USES_COMMENTS)
        {
            $remove_menus[] = 'edit-comments.php';
        }

        // don't need tags
        if (!APP_OPT_USES_TAGS && APP_OPT_USES_POSTS)
        {
            $remove_submenus[] = array('parent' => 'edit.php', 'child' => 'edit-tags.php?taxonomy=post_tag');
        }

        // don't need media library
        if (!APP_OPT_USES_LIBRARY)
        {
            $remove_menus[] = 'upload.php';
        }

        // remove stuff that clients don't need to mess with
        if (APP_OPT_CLIENT_MODE)
        {
            $remove_submenus[] = array('parent' => 'themes.php', 'child' => 'themes.php');
        }

        if (count($remove_menus))
        {
            foreach ($remove_menus as $menu_to_remove)
            {
                remove_menu_page($menu_to_remove);
            }
        }

        if (count($remove_submenus))
        {
            foreach ($remove_submenus as $menu_to_remove)
            {
                $menu_to_remove = (object)$menu_to_remove;
                remove_submenu_page($menu_to_remove->parent, $menu_to_remove->child);
            }
        }
    }

    /**
     * Add style and javascript to WordPress Admin
     *
     * @return void
     */
    public function admin_head()
    {
        // add some already available javascript components
        wp_enqueue_script('jquery-ui-datepicker');

        // add our own components
        $framework_path = APP_FRAMEWORK_URL . '/assets';
        $framework_scripts_path = $framework_path . '/js';
        $framework_styles_path = $framework_path . '/css';

        // add our own scripts
        $scripts = array(
           'admin.js',
        );
        \t1e\register_scripts($scripts, $framework_scripts_path);

        // add our own styles
        \t1e\register_style('t1e-css-jquery-jquery-ui-datepicker', $framework_styles_path . '/jquery/jquery-ui/datepicker.css');
        \t1e\register_style('t1e-css-admin', $framework_styles_path . '/admin.css');
    }

    /**
     * Edit WordPress footer
     *
     * @return string  A text string to display in the footer or null for nothing
     */
    public function admin_footer_text($default)
    {
        return null;
    }

    ///////////////////
    // Post listings //
    ///////////////////

    /**
     * Order custom post types by title by default
     *
     * @param WP_Query $query
     *
     * @return WP_Query
     */
    public function pre_get_posts($query)
    {
        if (!$query->is_main_query()) { return; }
        $post_type = $query->query['post_type'];
        $registry = Registry::get_instance();
        if (!empty($registry->modules[$post_type]))
        {
            $module = $registry->modules[$post_type];
            if ($module['list_orderby'])
            {
                $query->set('orderby', $module['list_orderby']);
                $query->set('order', 'ASC');
            }
            if ($module['list_order']) { $query->set('order', $module['list_order']); }
        }
        return $query;
    }

    /**
     * Display information about the featured image upload
     *
     * @param string $content  Content to display with image upload
     * @param int $post_id     ID of post we are editing
     */
    public function admin_post_thumbnail_html($content, $post_id)
    {
        $registry = Registry::get_instance();
        $post_type = get_post_type($post_id);
        $add_content = '';
        if (!empty($registry->modules[$post_type]))
        {
            $module = (object)$registry->modules[$post_type];
            if (isset($module->featured_image_template))
            {
                $add_content .= '<p class="t1e-featured-image-template">';
                $add_content .=     __('Image Template: ', APP_LANG);
                $add_content .=     '<span>';
                $add_content .=         $module->featured_image_template;
                $add_content .=     '</span>';
                $add_content .= '</p>';
            }
        }
        return $add_content . $content;
    }

    /**
     * Adds content to custom columns
     *
     * @param string $column_name  Current column name in list
     * @param int $id              ID of current item in list
     *
     * @return void
     */
    public function manage_posts_custom_column($column_name, $id)
    {
        if ($column_name == 'post-thumbs')
        {
            echo the_post_thumbnail('admin-thumbnail');
        }
    }

    /**
     * Adds custom columns to post type listings
     *
     * TODO: could move to \t1e\admin\Meta
     *
     * @param array $post_columns  Default post columns titles
     * @param string $post_type    Post type to add columns to listing
     *
     * @return array               New post column titles
     */
    public function manage_posts_columns($post_columns, $post_type)
    {
        $registry = Registry::get_instance();
        $fields_to_slice = $this->meta->get_list_columns_for_post_type($post_type);

        $fields_to_slice = array_map(function($arr) {
            return $arr['title'];
        }, $fields_to_slice);


        if (count($fields_to_slice))
        {
            $post_columns = array_slice($post_columns, 0, 3, true) +
                $fields_to_slice +
                array_slice($post_columns, 1, null, true);
        }

        if (post_type_supports($post_type, 'thumbnail'))
        {
            $post_columns = array_slice($post_columns, 0, 1, true) +
                array('post-thumbs' => __('Thumb', APP_LANG)) +
                array_slice($post_columns, 1, null, true);
        }

        if ($post_type != 'post')
        {
            $unset_date = true;
            if (!empty($registry->modules[$post_type]))
            {
                $module = $registry->modules[$post_type];
                $unset_date = isset($module['list_date']) ? !(bool)$module['list_date'] : true;
            }
            if ($unset_date) { unset($post_columns['date']); }
        }

        return $post_columns;
    }

    ////////////////////
    // Saving content //
    ////////////////////

    /**
     * Perform actions when a post is saved
     *
     * Action signature: do_action('save_post', $post_ID, $post, $update)
     *
     * @param int $post_id  The ID of the post being updated or saved
     * @param object $post  Post being saved
     * @param int $update   1 if post is being updated, 0 otherwise
     *
     * @return int          The ID of the post being updated or saved
     */
    public function save_post($post_id, $post, $update)
    {
        // verify that this isn't an auto save routine
        // if it is our form has not been submitted, so we dont want to do anything
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return $post_id;

        // verify that this isn't a quick edit routine
        if (defined('DOING_AJAX') && DOING_AJAX) return $post_id;

        // check permissions
        if (!current_user_can('edit_post', $post_id)) return $post_id;

        // no need to process certain post types
        if (in_array($post->post_type, array('revision', 'nav_menu_item', 'attachment')))
        {
            return $post_id;
        }

        // get fields auto-generated by registering with t1e system
        $registry = Registry::get_instance();
        isset($registry->fields_to_save) || $registry->fields_to_save = array();
        $fields = $registry->fields_to_save;

        // add default fields to save
        $fields = array_unique(array_merge($fields, array('relationships')));

        // get additional fields assigned my user in functions.php
        $fields_func = APP_LANG . '_fields_to_save';
        if (function_exists($fields_func))
        {
            $fields = array_unique(array_merge($fields, (array)call_user_func($fields_func)));
        }

        // just in case we don't have any fields to save
        if (empty($fields)) return $post_id;

        // update the metadata
        foreach ($fields as $field)
        {
            // echo "Saving field for post with ID: {$post_id} field: {$field}<br>";
            $this->meta->update_post_meta_by_key($post_id, $field);
        }

        // pr($_POST); pr($fields); exit();

        return $post_id;
    }
}

endif;
