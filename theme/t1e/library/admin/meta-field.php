<?php

/**
 * Contains classes for creating metadata fields
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\admin\meta_field;

use \t1e\core\FieldTypes as FieldTypes;


require_once APP_LIBRARY . '/core/field-types.php';


if (!class_exists(__NAMESPACE__ . '\MetaField')):

/**
 * Base class for a meta field
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
abstract class MetaField
{
    /**
     * Add a blank item to select input
     *
     * @var bool
     */
    protected $_allow_blank = true;

    /**
     * Value of the field
     *
     * @var string
     */
    protected $_current_value = '';

    /**
     * Whether the title of the field should be "inflected"
     *
     * @var bool
     */
    protected $_inflect_title = true;

    /**
     * Name property of the field
     *
     * @var string
     */
    protected $_name = null;

    /**
     * Options for rendering the field
     *
     * @var array
     */
    protected $_options = array();

    /**
     * Placeholder text to show in field if supported
     *
     * @var string
     */
    protected $_placeholder;

    /**
     * Title of the field
     *
     * @var string
     */
    protected $_title = '';

    /**
     * Type of field
     *
     * @see \t1e\core\FieldTypes
     * @var string
     */
    protected $_type = FieldTypes::TEXT;

    /**
     * Possible values for the input (such as a select input)
     *
     * @var array
     */
    protected $_values = array();

    /**
     * Create a MetaField instance
     *
     * @param string $name  Name property for the input
     * @param array $options  Additional options for rendering the input field
     */
    public function __construct($name, array $options = array())
    {
        $this->_name = $name;
        $this->_options = $options;

        foreach ($options as $key => $value)
        {
            if (property_exists($this, "_{$key}"))
            {
                if (!is_null($value))
                {
                    $this->{'_' . $key} = $value;
                }
            }
        }

        if (is_null($this->_placeholder))
        {
            $this->_placeholder = \t1e\core\Inflector::humanize($this->_title);
        }
    }

    /**
     * Renders the field type input with a title
     *
     * @param bool $echo  Echo the content if true, return otherwise
     *
     * @return string     Rendered input and title
     */
    public function render($echo = true)
    {
        $res  = '';

        $res .= $this->_render_item('_do_title');
        $res .= $this->_render_item('_do_input');

        if ($echo)
        {
            echo $res;
            return;
        }

        return $res;
    }

    /**
     * Displays the Meta Field title
     */
    protected function _do_title()
    {
    ?>
    <p>
        <strong><?php echo $this->_title; ?></strong>
    </p>
    <?php
    }

    /**
     * Renders a meta field item like the title or input
     *
     * @param function $callback  The function to call to render the item
     * @param bool $echo          Echo to display if true, return otherwise
     *
     * @return string|void        String if $echo is true, void otherwise
     */
    protected function _render_item($callback, $echo = false)
    {
        ob_start();

        $this->$callback();

        $out = ob_get_contents();
        ob_end_clean();

        if ($echo)
        {
            echo $out;
            return;
        }

        return $out;
    }

    /**
     * Renders the field input
     */
    protected function _do_input()
    {
    ?>
    <input value="<?php echo esc_attr($this->_current_value); ?>" type="<?php echo esc_attr($this->_type); ?>" name="<?php echo esc_attr($this->_name); ?>" id="<?php echo esc_attr($this->_name); ?>" placeholder="<?php echo esc_attr($this->_placeholder); ?>"/>
    <?php
    }
}

endif;


if (!class_exists(__NAMESPACE__ . '\Text')):

/**
 * Text meta field
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Text extends MetaField
{
}

endif;


if (!class_exists(__NAMESPACE__ . '\Textarea')):

/**
 * Textarea meta field
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Textarea extends MetaField
{
    protected $_type = FieldTypes::TEXTAREA;

    protected function _do_input()
    {
    ?>
    <textarea cols="60" rows="6" name="<?php echo esc_attr($this->_name); ?>" id="<?php echo esc_attr($this->_name); ?>"  placeholder="<?php echo esc_attr($this->_placeholder); ?>"><?php echo $this->_current_value; ?></textarea>
    <?php
    }
}

endif;


if (!class_exists(__NAMESPACE__ . '\Option')):

/**
 * Option meta field
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Option extends MetaField
{
    protected $_type = FieldTypes::OPTION;

    protected function _do_input()
    {
    ?>
    <label for="<?php echo esc_attr($this->_name); ?>">
        <input<?php if ($this->_current_value) echo " checked=\"checked\""; ?> value="1" type="checkbox" name="<?php echo esc_attr($this->_name); ?>" id="<?php echo esc_attr($this->_name); ?>"/>
        <?php echo $this->_title; ?>
    </label>
    <?php
    }

    // No title on option fields
    protected function _do_title() {}
}

endif;


if (!class_exists(__NAMESPACE__ . '\Select')):

/**
 * Select meta field
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Select extends MetaField
{
    protected $_type = FieldTypes::SELECT;

    protected function _do_input()
    {
        if (empty($this->_values)) return;
    ?>
    <select id="<?php echo esc_attr($this->_name); ?>" name="<?php echo esc_attr($this->_name); ?>" autocomplete="off">
    <?php
        if ($this->_allow_blank): ?>
        <option value=""></option>
    <?php
        endif;
        if (is_assoc($this->_values))
        {
            foreach ($this->_values as $value => $text): ?>
            <option<?php if ($this->_current_value == $value) echo " selected=\"selected\""; ?> value="<?php echo esc_attr($value); ?>"><?php echo $text; ?></option>
            <?php endforeach;
        }
        else
        {
            foreach ($this->_values as $value): ?>
            <option<?php if ($this->_current_value == $value) echo " selected=\"selected\""; ?> value="<?php echo esc_attr($value); ?>"><?php echo $value; ?></option>
            <?php endforeach;
        }
    ?>
    </select>
    <?php
    }
}

endif;


if (!class_exists(__NAMESPACE__ . '\Color')):

/**
 * Color meta field
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Color extends MetaField
{
    protected $_type = FieldTypes::COLOR;
}

endif;


if (!class_exists(__NAMESPACE__ . '\Date')):

/**
 * Date selector meta field
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Date extends MetaField
{
    protected $_type = FieldTypes::DATE;

    public function __construct($name, array $options = array())
    {
        parent::__construct($name, $options);

        if (empty($this->_current_value))
        {
            $this->_current_value = date('Y-m-d');
        }

        if (empty($this->_placeholder))
        {
            $this->_placeholder = 'YYYY-MM-DD';
        }
    }
}

endif;


if (!class_exists(__NAMESPACE__ . '\Datepicker')):

/**
 * Datepicker meta field
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Datepicker extends Date
{
    protected $_type = FieldTypes::DATEPICKER;

    public function __construct($name, array $options = array())
    {
        parent::__construct($name, $options);

        if (empty($this->_current_value))
        {
            $this->_current_value = date('Y-m-d');
        }

        if (empty($this->_placeholder))
        {
            $this->_placeholder = 'YYYY-MM-DD';
        }
    }

    protected function _do_input()
    {
    ?>
    <div class="t1e-datepicker" data-alt-field="<?php echo esc_attr($this->_name); ?>">
        <input class="t1e-datepicker-input" value="<?php echo esc_attr($this->_current_value); ?>" type="hidden" name="<?php echo esc_attr($this->_name); ?>" id="<?php echo esc_attr($this->_name); ?>" placeholder="<?php echo esc_attr($this->_placeholder); ?>"/>
        <div class="t1e-datepicker-inline"></div>
    </div>
    <?php
    }
}

endif;


if (!class_exists(__NAMESPACE__ . '\Time')):

/**
 * Time selector meta field
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Time extends Select
{
    protected $_type = FieldTypes::TIME;

    public function __construct($name, array $options = array())
    {
        parent::__construct($name, $options);

        $values = array();
        $slots = array('00', '15', '30', '45');

        for ($i = 0; $i < 24; $i++)
        {
            foreach ($slots as $slot)
            {
                $period = $i >= 12 ? 'pm' : 'am';
                if ($i == 0 || $i > 12)
                    $hour = abs($i - 12);
                else
                    $hour = $i;
                // key is normal time
                $text = sprintf('%s:%s %s', str_pad($hour, 2, '0', STR_PAD_LEFT), $slot, $period);
                // value stored as military time
                $value = sprintf('%s:%s', str_pad($i, 2, '0', STR_PAD_LEFT), $slot);
                $values[$value] = $text;
            }
        }

        $this->_allow_blank = false;
        $this->_values = $values;

        if (empty($this->_current_value))
        {
            $this->_current_value = '12:00';
        }
    }
}

endif;


if (!class_exists(__NAMESPACE__ . '\Url')):

/**
 * URL meta field
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 * @todo       Validate URL on blur/focus
 */
class Url extends MetaField
{
    protected $_type = FieldTypes::TEXT;
}

endif;


if (!class_exists(__NAMESPACE__ . '\Email')):

/**
 * Email meta field
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Email extends MetaField
{
    protected $_type = FieldTypes::EMAIL;
}

endif;


if (!class_exists(__NAMESPACE__ . '\Filepicker')):

/**
 * Adds a file picker to custom post type meta fields
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Filepicker extends MetaField
{
    protected $_type = FieldTypes::TEXT;

    public function __construct($name, array $options = array())
    {
        if (is_null($this->_placeholder))
        {
            $this->_placeholder = __('Click browse to attach a file', APP_LANG);
        }

        parent::__construct($name, $options);
    }

    protected function _do_input()
    {
        $id = esc_attr($this->_name);
    ?>
    <table class="t1e-form-table t1e-form-table-filepicker" cellpadding="0" cellspacing="0">
        <tr>
            <td><input value="<?php echo esc_attr($this->_current_value); ?>" type="<?php echo esc_attr($this->_type); ?>" name="<?php echo $id; ?>" id="<?php echo $id; ?>" placeholder="<?php echo esc_attr($this->_placeholder); ?>"/></td>
            <td><input type="button" class="button-primary button" name="<?php echo $id; ?>_button" id="<?php echo $id; ?>_button" value="Browse"/></td>
            <td><input type="button" class="button" name="<?php echo $id; ?>_clear_button" id="<?php echo $id; ?>_clear_button" value="Clear"/></td>
        </tr>
    </table>
    <script>
        // Snagged from Zilla Framework
        jQuery(function($) {
            var frame;
            $('#<?php echo $id ?>_clear_button').on('click', function(event) {
                event.preventDefault();
                $('#<?php echo $id; ?>').val('');
            });
            $('#<?php echo $id ?>_button').on('click', function(event) {
                event.preventDefault();

                var options = {
                    state: 'insert',
                    frame: 'post'
                };

                frame = wp.media(options).open();

                var menu_view = frame.menu.get('view')
                  , toolbar_view = frame.toolbar.get('view')

                menu_view.unset('gallery')
                menu_view.unset('featured-image')
                menu_view.unset('embed')

                toolbar_view.set({
                    insert: {
                        style: 'primary',
                        text: '<?php _e("Attach File", APP_LANG); ?>',
                        click: function() {
                            var models = frame.state().get('selection'),
                                url = models.first().attributes.url;
                            $('#<?php echo $id; ?>').val(url);
                            frame.close();
                        }
                    }
                });

            });

        });
    </script>
    <?php
    }
}

endif;
