<?php

/**
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\admin;


{
    /**
     * @link \t1e\core\Plugin
     */
    require_once APP_LIBRARY . '/core/plugin.php';
}


if (!class_exists(__NAMESPACE__ . '\Page')):

/**
 * Base class for an admin Page
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
abstract class Page extends \t1e\core\Plugin
{
    /**
     * List of WordPress hooks to add at run time
     *
     * @var array
     * @see \t1e\core\Plugin::$_actions
     */
    protected $_actions = array(
        'admin_menu',
        'admin_init',
    );

    /**
     * Title of page in menu
     *
     * @var string
     */
    protected $_menu_title = '';

    /**
     * Title of page
     *
     * @var string
     */
    protected $_page_title = '';

    /**
     * Where are the views for this page
     *
     * @var string
     */
    protected $_view_location = null;

    /**
     * Setup the plugin and set the default view location
     *
     * @return void
     */
    protected function _setup()
    {
        parent::_setup();

        $this->_view_location = APP_LIBRARY . '/admin/views';
    }

    /**
     * Override this method to add pages to sidebar menu
     *
     * @return void
     */
    public function admin_menu()
    {
    }

    /**
     * Override this method to add functionality to admin
     *
     * @return void
     */
    public function admin_init()
    {
    }

    /**
     * Render a view template
     *
     * @param string $template  Name of template to render
     * @param array $data       Data to apply to template
     * @param bool $echo        Echo the rendered template if true, return otherwise
     *
     * @return mixed            String if $echo is true, void otherwise
     */
    protected function _render($template, $data = null, $echo = true)
    {
        $content = "";
        $view = "{$this->_view_location}/{$template}.php";

        if (file_exists($view))
        {
            if (is_array($data))
            {
                foreach ($data as $key => $value)
                {
                    $$key = $value;
                }
            }

            ob_start();
            include_once $view;
            $content = ob_get_contents();
            ob_end_clean();
        }

        if ($echo)
        {
            echo $content;
        }
        else
        {
            return $content;
        }
    }
}

endif;
