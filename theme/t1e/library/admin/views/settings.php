<?php

/**
 * admin/views/settings.php
 *
 * Presents the global website settings view. See below
 * for a list of parameters fed to this script.
 *
 * @var object $raw_data                Raw json parsed from <theme>/settings.json
 * @var object $settings                Saved settings from WordPress
 * @var array  $default_settings_keyed  List of default settings parsed from <theme>/settings.json
 * @var array  $default_settings        List of default settings parsed from <theme>/settings.json
 * @var string $option_group            Name of the settings group to save
 * @var string $values_group            Name of the values group to save
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 *
 */


use \t1e\core\FieldTypes as FieldTypes;


// echo "default_settings:"; pr($default_settings);
// echo "default_settings_keyed:"; pr($default_settings_keyed);
// echo "settings"; pr($settings);
// echo "option_group=$option_group<br>values_group=$values_group<br>";

$updated = isset($_REQUEST['settings-updated']) ? (bool)$_REQUEST['settings-updated'] : false;
if ($updated) { \t1e\clear_caches(); }

?>

<?php if ($updated): ?>
<div class="updated"><p><strong><?php _e('Options saved'); ?></strong></p></div>
<?php endif; // If the form has just been submitted, this shows the notification ?>

<div class="wrap">
    <h2>
        <?php echo wp_get_theme() . __(' Website Options', APP_LANG); ?>
    </h2>

    <form method="post" action="options.php">

        <?php

        // This function outputs some hidden fields required by the form,
        // including a nonce, a unique number used to ensure the form has been
        // submitted from the admin page and not somewhere else, for security
        settings_fields($option_group);

        ?>

        <table class="form-table">
            <?php
            foreach ($default_settings as $key => $value):
                $setting = (object)$value;
                $field_id = esc_attr($key);
                $field_name = esc_attr("{$values_group}[{$key}]");
                $field_value = isset($settings[$key]) ? $settings[$key] : '';
                $field_placeholder = esc_attr($setting->placeholder);
            ?>

            <tr>
                <th scope="row">
                    <?php if ($setting->type != FieldTypes::OPTION): ?>
                    <label for="<?php echo $field_id; ?>">
                        <?php echo $setting->label; ?>
                    </label>
                    <?php else: ?>
                    <?php echo $setting->label; ?>
                    <?php endif; ?>
                </th>
                <td>
                <?php
                    switch ($setting->type):

                        case FieldTypes::TEXTAREA:
                ?>
                    <textarea id="<?php echo $field_id; ?>" name="<?php echo $field_name; ?>" rows="5" cols="30" placeholder="<?php echo $field_placeholder; ?>"><?php echo stripslashes($field_value); ?></textarea>
                <?php
                            break;

                        case FieldTypes::OPTION:
                ?>
                <label for="<?php echo $field_id; ?>">
                    <input name="<?php echo $field_name; ?>" id="<?php echo $field_id; ?>" value="1" type="checkbox" <?php checked(true, $field_value); ?>/>
                    <?php echo (isset($setting->option_label) ? $setting->option_label : $setting->label); ?>
                </label>
                <?php
                            break;

                        case FieldTypes::SELECT:
                ?>
                <fieldset>
                    <?php
                    if ($setting->values):
                        foreach ($setting->values as $radio_key => $radio_val):
                    ?>
                    <label>
                        <input name="<?php echo $field_name; ?>" value="<?php esc_attr_e($radio_val); ?>" type="radio" <?php checked(esc_attr($radio_val), $field_value); ?>/>
                        <span><?php echo $radio_key; ?></span>
                    </label>
                    <br>
                    <?php
                        endforeach;
                    endif;
                    ?>
                </fieldset>
                <?php
                            break;

                        case FieldTypes::POST:
                            $post_type = isset($setting->post_type) ? $setting->post_type : 'post';
                            $post_select_opts = new \WP_Query(array('post_type' => $post_type, 'nopaging' => true));
                            wp_reset_query();
                ?>
                <select name="<?php echo $field_name; ?>" autocomplete="off">
                    <option value=""><?php echo esc_attr(__('Select Post')); ?></option>
                    <?php foreach ($post_select_opts->get_posts() as $post_select_opt): ?>
                    <option<?php if ($post_select_opt->ID == $field_value) echo ' selected="selected"' ?> value="<?php echo esc_attr($post_select_opt->ID); ?>"><?php echo $post_select_opt->post_title; ?></option>
                    <?php endforeach; ?>
                </select>
                <?php
                            break;

                        default:
                ?>
                    <input class="regular-text" id="<?php echo $field_id; ?>" name="<?php echo $field_name ?>" type="text" value="<?php esc_attr_e($field_value); ?>" placeholder="<?php echo $field_placeholder ?>"/>
                <?php
                    endswitch;

                    if (!is_null($setting->description)):
                ?>
                    <p class="description"><?php echo $setting->description; ?></p>
                <?php
                    endif;
                ?>
                </td>
            </tr>

            <?php
            endforeach;
            ?>
        </table>
        <?php submit_button(); ?>
    </form>
</div>
