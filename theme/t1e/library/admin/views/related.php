<?php

/**
 * admin/views/relationships.php
 *
 * @var string $parent_post_type  Name of post that relationships are children of
 * @var array $relationships      Registered relationships
 * @var array $metadata           Currently saved relationship meta-data
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


// WordPress hackery to make sure we maintain the global post object
global $post;
$tmp_post = $post;

$oid = "t1e-relationships-internal";
$scope = "t1e-{$parent_post_type}-relationships-div";

// echo $parent_post_type; pr($relationships); pr($metadata);

?>


<div id="<?php echo $oid; ?>" class="categorydiv <?php echo "{$oid}div"; ?>">
    <ul id="<?php echo $oid; ?>-tabs" class="category-tabs <?php echo "{$oid}-tabs"; ?>">
        <?php
            $i = 0;
            foreach ($relationships as $post_type => $rel):
                $class = ($i) ? 'hide-if-no-js' : 'tabs';
                $post_type_obj = get_post_type_object($post_type);
        ?>
        <li class="<?php echo $class; ?>">
            <a href="#t1e-relationship-<?php echo $post_type ;?>">
                <?php echo $post_type_obj->labels->name; ?>
            </a>
        </li>
        <?php
                $i++;
            endforeach;
        ?>
    </ul>

    <?php
        $i = 0;
        foreach ($relationships as $post_type => $rel):
            $style = $i > 0 ? ' style="display: none;"' : '';
            $id_prefix = "t1e-relationship-{$post_type}";
            $name_prefix = "{$parent_post_type}_{$post_type}";
            $included = isset($metadata[$name_prefix]) ? $metadata[$name_prefix] : array();
    ?>
    <div id="<?php echo $id_prefix; ?>" class="tabs-panel"<?php echo $style; ?>>
        <?php
            $related_posts = \t1e\get_posts($post_type, array('orderby' => 'title'));
            $posts = $related_posts->get_posts();

            if ($related_posts->post_count):
            ?>
            <ul id="relationship-checklist-<?php echo $post_type; ?>" data-wp-lists="list:relationships" class="relationship-checklist form-no-clear">
            <?php
                foreach ($posts as $post):
                    setup_postdata($post);

                    $id = get_the_ID();
                    $checked = in_array($id, $included);
                    $checked_str = $checked ? ' checked="checked"' : '';
                ?>
                <li id="<?php echo $name_prefix; ?>-1" class="<?php echo $id_prefix; ?>">
                    <label class="selectit">
                        <input<?php echo $checked_str; ?> value="<?php echo $id; ?>" name="relationships[<?php echo $name_prefix; ?>][]" id="<?php echo "{$id_prefix}-id-"; ?><?php $id; ?>" type="checkbox"/>
                        <?php the_title(); ?>
                    </label>
                </li>
                <?php
                endforeach;
            ?>
            </ul>
            <?php
            endif;
            $post = $tmp_post;
        ?>
    </div>
    <?php
            $i++;
        endforeach;
    ?>
</div>
