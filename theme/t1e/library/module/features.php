<?php

/**
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\module;

use \t1e\core\FieldTypes as FieldTypes;


{
    /**
     * @link \t1e\core\FieldTypes
     */
    require_once APP_LIBRARY . '/core/field-types.php';

    /**
     * @link \t1e\core\Module
     */
    require_once APP_LIBRARY . '/core/module.php';
}


if (!class_exists(__NAMESPACE__ . '\Features')):

/**
 * Adds the Features Module to the App's out-of-the-box modules
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Features extends \t1e\core\Module
{
    protected $_description = 'Marquee or feature rotator; ideal for home page';
    protected $_is_sortable = true;
    protected $_options = array('menu_icon' => 'dashicons-star-filled');
    protected $_type = 'features';

    public function get_meta_box()
    {
        return array(
            'caption' => array(
                'placeholder' => __('Caption or Subtitle', APP_LANG),
            ),
            'feature_url',
            'opens_in_blank_window' => FieldTypes::OPTION,
        );
    }
}

endif;
