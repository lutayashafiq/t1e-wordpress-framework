<?php

/**
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\client;


{
    /**
     * @link \t1e\client\Attachments
     */
    require_once APP_LIBRARY . '/client/attachments.php';

    /**
     * @link \t1e\client\Media
     */
    require_once APP_LIBRARY . '/client/media.php';

    /**
     * @link \t1e\client\Theme
     */
    require_once APP_LIBRARY . '/client/sorter.php';

    /**
     * @link \t1e\client\Theme
     */
    require_once APP_LIBRARY . '/client/theme.php';

    /**
     * @link \t1e\client\Types
     */
    require_once APP_LIBRARY . '/client/types.php';

    /**
     * @link \t1e\core\Plugin
     */
    require_once APP_LIBRARY . '/core/plugin.php';
}


if (!class_exists(__NAMESPACE__ . '\Core')):

/**
 * Bootstraps a front-facing WordPress theme (client).
 * This is also included when running in the admin panel.
 *
 * A `client\Core` instance is assigned to `App`’s
 * $client property. `App` also forwards any method calls
 * on to its `client\Core` instance.
 *
 * The client core is used to work with themes, custom post
 * types, attachments, and media, among other things. The core
 * using object composition and assigns a number of objects to
 * its member variables so each functionality is encapsulated
 * but fully accessible by the Core and the App.
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Core extends \t1e\core\Plugin
{
    /**
     * @var \t1e\client\Attachments
     */
    public $attachments;

    /**
     * @var \t1e\client\Media
     */
    public $media;

    /**
     * @var \t1e\client\Theme
     */
    public $theme;

    /**
     * @var \t1e\client\Types
     */
    public $types;

    /**
     * WordPress actions
     *
     * @var array
     */
    protected $_actions = array(
        'init',
        'wp_enqueue_scripts' => 'enqueue_scripts',
        'nav_menu_item_id' => array(10, 3)
    );

    /**
     * WordPress filters
     *
     * @var array
     */
    protected $_filters = array(
        'language_attributes',
    );

    //////////////////////
    // Instance methods //
    //////////////////////

    /**
     * Override the _setup method
     *
     * @return void
     */
    protected function _setup()
    {
        // Create the Client Core objects
        $this->attachments  = new Attachments();
        $this->media        = new Media();
        $this->theme        = new Theme();
        $this->types        = new Types();

        // Add support for custom backgrounds
        if (APP_OPT_USE_CUSTOM_BACKGROUNDS)
        {
            add_custom_background();
        }

        // Use featured/thumbnails for pages and posts.
        if (APP_OPT_USE_THUMBNAILS)
        {
            add_theme_support('post-thumbnails');

            add_image_size('admin-thumbnail', 65, 65);
            add_image_size('mobile-asset', 320);
            add_image_size('desktop-asset', 940);
        }

        // Automatically displays feed RSS links
        if (APP_OPT_USE_AUTOMATIC_FEED_LINKS)
        {
            add_theme_support('automatic-feed-links');
        }

        // Leave HTML alone
        if (APP_OPT_DONT_CLEAN_CONTENT)
        {
            remove_filter('the_content', 'wpautop');
            remove_filter('the_excerpt', 'wpautop');
        }
    }

    /////////////////////////////
    // Public instance methods //
    /////////////////////////////

    public function testing()
    {
        echo "Hello, world!";
    }

    /////////////////////
    // WordPress hooks //
    /////////////////////

    /**
     * Initializes the client theme
     *
     * @return void
     */
    public function init()
    {
        if (!APP_OPT_USE_VERSIONING)
        {
            wp_deregister_script('autosave');
        }

        if (!\t1e\is_admin_or_login())
        {
            remove_action('wp_head', 'wp_generator');

            if (!APP_OPT_USE_AUTOMATIC_FEED_LINKS)
            {
                remove_action('wp_head', 'rsd_link');
                remove_action('wp_head', 'wlwmanifest_link');
            }

            wp_deregister_script('jquery');
            wp_deregister_script('l10n');
        }

        add_post_type_support('page', 'excerpt');
    }

    /**
     * Adds default stylesheet and javascripts to theme
     *
     * @return void
     */
    public function enqueue_scripts()
    {
        $theme_name = wp_get_theme()->get_stylesheet();

        wp_enqueue_style($theme_name . '-style', get_stylesheet_uri(), array(), APP_VERSION);
    }

    /**
     * Modify HTML language attributes
     *
     * @param string $output  Output sent through by WordPress so we can modify it
     *
     * @return string
     */
    public function language_attributes($output)
    {
        // add Facebook and MS namespaces
        $namespaces = 'itemscope itemtype="http://schema.org/' . APP_SCHEMA_TYPE . '"';

        return "{$output} {$namespaces}";
    }

    /**
     * Adds menu-item-# ID to the menu item list items.
     *
     * @param int $id
     * @param object $item
     * @param array $args
     *
     * @return mixed int|string   ID for HTML menu object
     */
    public function nav_menu_item_id($id, $item, $args)
    {
        if (empty($item->title)) return $id;
        $theme_location = $args->theme_location ? $args->theme_location . '-' : '';
        return 'menu-item-' . $theme_location . strtolower(preg_replace('/[^\w]/i', '-', $item->title));
    }

    /**
     * Returns saved settings
     *
     * @return array
     */
    public function get_settings()
    {
        $key = APP_LANG . '_settings';
        $settings = get_option($key);
        return $settings;
    }
}

endif;
