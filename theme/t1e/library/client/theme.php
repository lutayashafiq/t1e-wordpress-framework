<?php

/**
 * Contains general global functions for use in theme template files
 *
 * Also contains the \t1e\client\Theme class that has methods for
 * working with themes from the functions.php file via the App
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\client;


/**
 * @link \t1e\core\Plugin
 */
require_once APP_LIBRARY . '/core/plugin.php';


if (!class_exists(__NAMESPACE__ . '\Theme')):

/**
 * Contains helper methods for working with themes in T1E Framework
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Theme extends \t1e\core\plugin
{
    /**
     * Default User Agent
     *
     * @var string
     */
    const DEFAULT_USER_AGENT = 'unknown';

    /**
     * Add short codes to theme
     *
     * @var array
     */
    protected $_shortcodes = array(
        'home_url',
    );

    /////////////////////////////
    // Public instance methods //
    /////////////////////////////

    /**
     * Helper for adding menus
     *
     * @param array $menus  Array of menus to add
     */
    public function add_menus(array $menus)
    {
        register_nav_menus($menus);
    }

    /**
     * Alias of `\t1e\register_style`
     *
     * @see \t1e\register_style
     */
    public function register_style($handle, $source = null, $enqueue = true)
    {
        \t1e\register_style($handle, $source, $enqueue);
    }

    /**
     * Alias of `\t1e\register_script`
     *
     * @see \t1e\register_script
     */
    public function register_script($file_name, $version = null, $in_footer = true, $enqueue = true)
    {
        \t1e\register_script($file_name, $version, $in_footer, $enqueue);
    }

    /**
     * Registers javascript scripts with WordPress
     *
     * @param array $scripts  An array of paths to javascript files
     */
    public function register_scripts(array $scripts)
    {
        \t1e\register_scripts($scripts);
    }

    /**
     * Short code for getting home url
     *
     * @return string  Home URL
     */
    public function home_url_shortcode()
    {
        return home_url();
    }
}

endif;


namespace t1e;


if (!function_exists(__NAMESPACE__ . '\is_admin_or_login')):

/**
 * Determines if view is either an admin page or a login page
 *
 * @return bool  true if page is admin or login, false otherwise
 */
function is_admin_or_login()
{
    return is_admin() || in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'));
}

endif;


if (!function_exists(__NAMESPACE__ . '\is_blog_page')):

/**
 * Conditional test to see if current query is related to blog
 *
 * @return bool  true if blog landing page or blog archive
 */
function is_blog_page()
{
    return is_home() || is_archive();
}

endif;


if (!function_exists(__NAMESPACE__ . '\get_post_name')):

/**
 * Return the slug name of the post
 *
 * @param int $id  ID of post to get name of
 *
 * @return string  Slug name of post
 */
function get_post_name($id = 0)
{
    $post = get_post($id);
    if (!empty($post))
    {
        $post_name = isset($post->post_name) ? $post->post_name : '';
        $id = isset($post->ID) ? $post->ID : 0;

        /**
         * Filter the post name.
         *
         * @param string $post_name The post name/slug.
         * @param int    $id        The post ID.
         */
        return apply_filters('the_post_name', $post_name, $id);
    }
}

endif;


if (!function_exists(__NAMESPACE__ . '\the_post_name')):

/**
 * Displays the slug name of the post
 *
 * @param int $id  ID of post to get name of
 */
function the_post_name($id = 0)
{
    echo get_post_name($id);
}

endif;


if (!function_exists(__NAMESPACE__ . '\page_url')):

/**
 * Displays the page url
 */
function page_url()
{
    echo get_page_url();
}

endif;


if (!function_exists(__NAMESPACE__ . '\get_page_url')):

/**
 * Fetch the URL of the current page
 *
 * @return string  Current page URL
 */
function get_page_url()
{
    $pageURL = 'http';
    if ($_SERVER['HTTPS'] == 'on') {$pageURL .= 's';}
    $pageURL .= '://';
    if ($_SERVER['SERVER_PORT'] != '80')
    {
        $pageURL .= $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
    }
    else
    {
        $pageURL .= $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    }
    return $pageURL;
}

endif;


if (!function_exists(__NAMESPACE__ . '\get_user_agent')):

/**
 * Returns the name of the platform for use as a CSS class or other use
 *
 * @param string $agent  Default agent name
 *
 * @return string User agent device strings
 */
function get_user_agent($agent = \t1e\client\Theme::DEFAULT_USER_AGENT)
{
    $ua = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'cli';

    switch (1)
    {
        case (preg_match('/iphone|ipod/i', $ua)):
            $agent = 'mobile iOS';
            break;
        case (preg_match('/ipad/i', $ua)):
            $agent = 'mobile tablet iPad';
            break;
        case (preg_match('/android/i', $ua)):
            $agent = 'mobile Android';
            break;
        case (preg_match('/webos/i', $ua)):
            $agent = 'mobile webOS';
            break;
        case (preg_match('/blackberry/i', $ua)):
            $agent = 'mobile Blackberry';
            break;
    }

    // Figure out which browser the user is using
    if (preg_match('/Firefox/i', $ua))
    {
        $agent = 'ff';
        preg_match('/Firefox\/((\d+)\.(\d+))/i', $ua, $versioning);
        if (!empty($versioning[2])) $agent .= " ff{$versioning[2]}";
    }
    else if (preg_match('/MSIE/i', $ua))
    {
        $agent = 'ie';
        preg_match('/MSIE.(\d+)/i', $ua, $versioning);
        if (!empty($versioning[1])) $agent .= " ie{$versioning[1]}";
    }
    else if (preg_match('/Chrome/i', $ua))
    {
        $agent = 'chrome';
        preg_match('/Chrome\/((\d+)\.(\d+))/i', $ua, $versioning);
        if (!empty($versioning[2])) $agent .= " chrome{$versioning[2]}";
    }
    else if (preg_match('/Safari/i', $ua))
    {
        $agent = 'safari';
        preg_match('/Version\/(\d+)\./i', $ua, $versioning);
        if (!empty($versioning[1])) $agent .= " safari{$versioning[1]}";
    }
    else if (preg_match('/Opera/i', $ua))
    {
        preg_match('/Version\/(?P<version>[\d]+)\./i', $ua, $matches);
        $agent = 'opera';
        if (isset($matches['version']))
        {
            $agent .= " opera" . $matches['version'];
        }
    }

    if (preg_match('/Macintosh;/i', $ua)) $agent .= ' mac';
    if (preg_match('/Windows /i', $ua)) $agent .= ' windows';
    if (preg_match('/Windows NT 5.1|Windows XP|Windows NT 5.2/i', $ua)) $agent .= ' windows-xp';
    if (preg_match('/Windows NT 6.0/i', $ua)) $agent .= ' windows-vista';
    if (preg_match('/Windows NT 6.1/i', $ua)) $agent .= ' windows-7';
    if (preg_match('/Windows NT 6.2/i', $ua)) $agent .= ' windows-8';

    return $agent;
}

endif;


if (!function_exists(__NAMESPACE__ . '\user_agent')):

/**
 * Prints user agent class names
 *
 * @param string $agent  Default agent strings if unknown agent
 */
function user_agent($agent = \t1e\client\Theme::DEFAULT_USER_AGENT)
{
    echo get_user_agent($agent);
}

endif;


if (!function_exists(__NAMESPACE__ . '\content_nav')):

/**
 * Generates the "next/prev" navigation links for a blog feed.
 *
 * @param array $options Options for navigation.
 *              'nav_id': ID to use for the navigation HTML element.
 *              'newer': Text to display for the "next" link.
 *              'older': Text to display for the "prev" link.
 *              'separator': Text to use to separate links.
 */
function content_nav($options = null)
{
    global $wp_query;

    if ($options === null)
    {
        $options = array();
    }

    $defaults = array(
        'newer'        => 'Newer Posts',
        'older'        => 'Older Posts',
        'newer_symbol' => '&rarr;',
        'older_symbol' => '&larr;',
        'separator'    => '',
        'nav_id'       => 'content-nav'
    );
    $opts = array_merge($defaults, $options);

    $newer = $opts['newer'];
    if (strlen($opts['newer_symbol']) > 0)
    {
        $newer .= ' <span class="meta-nav">' . $opts['newer_symbol'] . '</span>';
    }

    $older = '';
    if (strlen($opts['older_symbol']) > 0)
    {
        $older = '<span class="meta-nav">' . $opts['older_symbol'] . '</span> ';
    }
    $older .= $opts['older'];

    $next_link = get_next_posts_link(__($older, APP_LANG));
    $prev_link = get_previous_posts_link(__($newer, APP_LANG));

    if ($wp_query->max_num_pages > 1): ?>
        <nav id="<?php echo $opts['nav_id']; ?>" class="dirnav">
            <p>
                <span class="dirlink nav-previous <?php if (!$next_link) echo 'hidden'; ?>"><?php echo ($next_link) ? $next_link : '<span class="nothing"></span>'; ?></span>
                <span class="separator"><?php echo $separator; ?></span>
                <span class="dirlink nav-next <?php if (!$prev_link) echo 'hidden'; ?>"><?php echo ($prev_link) ? $prev_link : '<span class="nothing"></span>'; ?></span>
            </p>
        </nav><!-- #nav-above -->
    <?php endif;
}

endif;


if (!function_exists(__NAMESPACE__ . '\get_setting')):

/**
 * Get a setting from the theme's global settings
 *
 * @param string $key   Name of setting to get
 * @param bool   $echo  Render content to output if true, return if false
 *
 * @return mixed|null  Value of setting or null if not set or $echo is true
 */
function get_setting($key, $echo = true)
{
    $settings_key = APP_LANG . '_settings';
    $settings = get_option($settings_key);
    if ($settings && isset($settings[$key]))
    {
        if ($echo) {
            echo $settings[$key];
            return;
        }
        return $settings[$key];
    }
}

endif;


if (!function_exists(__NAMESPACE__ . '\nav')):

/**
 * Displays a WordPress navigation container
 *
 * @param string $location    Name of the nav location
 * @param string $container   Wrap the nav in HTML container of this type
 * @param string $menu_class  Class to apply to $container
 */
function nav($location, $container = 'nav', $menu_class = 'nav-menu')
{
    wp_nav_menu(array(
        'theme_location' => $location,
        'container'      => $container,
        'menu_class'     => $menu_class,
    ));
}

endif;


if (!function_exists(__NAMESPACE__ . '\time_ago')):

/**
 * TODO: docs
 */
function time_ago($time)
{
    $time = time() - $time; // to get the time since that moment

    $tokens = array (
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    );

    foreach ($tokens as $unit => $text)
    {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits . ' ' . $text . (($numberOfUnits>1) ? 's' : '');
    }
}

endif;


if (!function_exists(__NAMESPACE__ . '\register_style')):

/**
 * TODO: docs
 */
function register_style($handle, $source = null, $enqueue = true)
{
    wp_register_style($handle, $source);
    if ($enqueue) wp_enqueue_style($handle);
}

endif;


if (!function_exists(__NAMESPACE__ . '\register_script')):

/**
 * Wrapper for WordPress' javascript registration
 * @param string      $file_name Name of the script. Should be unique.
 * @param string|bool $version   Optional. String specifying script version number, if it has one, which is concatenated
 *                               to end of path as a query string. If no version is specified or set to false, a version
 *                               number is automatically added equal to current installed WordPress version.
 *                               If set to null, no version is added. Default 'false'. Accepts 'false', 'null', or 'string'.
 * @param bool        $in_footer Optional. Whether to enqueue the script before </head> or before </body>.
 *                               Default 'false'. Accepts 'false' or 'true'.
 * @param bool        $enqueue   Optional. Whether to enqueue the script.
 *                               Default 'true'. Accepts 'false' or 'true'.
 * @param string      $js_loc    Path to javascripts. If set to null, defaults to value of APP_SCRIPTS_URL constant.
 */
function register_script($file_name, $version = null, $in_footer = true, $enqueue = true, $js_loc = null)
{
    if (substr($file_name, 0, 2) === '//')
    {
        $file_path = $file_name;
    }
    else
    {
        $file_path = trailingslashit(APP_SCRIPTS_URL) . $file_name;
        if (!is_null($js_loc))
        {
            $file_path = trailingslashit($js_loc) . $file_name;
        }
    }
    wp_register_script($file_name, $file_path, null, $version, $in_footer);
    if ($enqueue) wp_enqueue_script($file_name);
}

endif;


if (!function_exists(__NAMESPACE__ . '\register_scripts')):

/**
 * TODO: docs
 */
function register_scripts(array $scripts, $js_loc = null)
{
    $defaults = array('version' => null, 'in_footer' => true, 'enqueue' => true, 'js_loc' => $js_loc);
    foreach ($scripts as $file_name => $options)
    {
        if (is_numeric($file_name))
        {
            $file_name = $options;
            $args = $defaults;
        }
        else
        {
            $args = array_merge($defaults, (array)$options);
        }
        $args = (object)$args;
        register_script($file_name, $args->version, $args->in_footer, $args->enqueue, $args->js_loc);
    }
}

endif;


if (!function_exists(__NAMESPACE__ . '\setup')):

/**
 * Helper method to setup post data manually with WordPress
 *
 * If `$the_post` isn't supplied, falls back to global `$post` object
 *
 * @param WP_Post $the_post  Manual post object to setup
 */
function setup($the_post = null)
{
    global $post;
    if (is_null($the_post)) { $the_post = $post; }
    setup_postdata($the_post);
}

endif;


if (!function_exists(__NAMESPACE__ . '\reset')):

/**
 * TODO: docs
 */
function reset()
{
    wp_reset_postdata();
}

endif;


if (!function_exists(__NAMESPACE__ . '\theme_has_seo_plugin')):

/**
 * Test if the theme is utilizing any sort of SEO plugin
 * that could conflict with the base meta configuration.
 *
 * @return bool  True if conflicting plugin is installed, false otherwise
 */
function theme_has_seo_plugin()
{
    $has_seo = false;
    // test for activated Yoast WP SEO Plugin
    if (defined('WPSEO_VERSION')) $has_seo = true;
    return $has_seo;
}

endif;


if (!function_exists(__NAMESPACE__ . '\url_starts_with')):

/**
 * Tests if the current page URL starts with <code>$start</code>.
 *
 * @param string $start String to look for.
 * @return bool <code>true</code> if the current URL begins with <code>$start</code>.
 */
function url_starts_with($start)
{
    return strpos($_SERVER['REQUEST_URI'], $start) === 0;
}

endif;


if (!function_exists(__NAMESPACE__ . '\clear_caches')):

/**
 * Clears cached files from recognized cache plugins
 */
function clear_caches()
{
    if (function_exists('wp_cache_clear_cache'))
    {
        wp_cache_clear_cache();
    }
    elseif (function_exists('w3tc_pgcache_flush'))
    {
        w3tc_pgcache_flush();
    }
}

endif;
