<?php

/**
 * Included in the client and admin, this file includes
 * functionality for managing file uploads and attachments.
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\client;


{
    /**
     * @link \t1e\core\Plugin
     */
    require_once APP_LIBRARY . '/core/plugin.php';
}


if (!class_exists(__NAMESPACE__ . '\Attachments')):

/**
 * TODO: docs
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Attachments extends \t1e\core\Plugin
{
    /**
     * TODO: docs
     */
    protected $_filters = array(
        'wp_get_attachment_url',
        'wp_handle_upload',
    );

    /**
     * TODO: docs
     */
    public function wp_handle_upload($info)
    {
        $info['url'] = \t1e\get_relative_attachment_path($info['url']);
        return $info;
    }

    /**
     * TODO: docs
     */
    public function wp_get_attachment_url($url)
    {
        return \t1e\get_relative_attachment_path($url);
    }
}

endif;


namespace t1e;


if (!function_exists(__NAMESPACE__ . '\get_relative_attachment_path')):

/**
 * TODO: docs
 */
function get_relative_attachment_path($path)
{
    $paths = (object)parse_url($path);
    return $paths->path;
}

endif;


if (!function_exists(__NAMESPACE__ . '\get_featured_image_src')):

/**
 * Returns the featured image post thumbnail
 *
 * If no `$post_id` specified, uses the current global `$post`
 *
 * @param integer $post_id  ID of post to get thumb for
 *
 * @return string  Source path of image thumbnail
 */
function get_featured_image_src($post_id = null)
{
    global $post;
    if (empty($post_id)) $post_id = $post->ID;
    return wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'single-post-thumbnail');
}

endif;


if (!function_exists(__NAMESPACE__ . '\get_image_attachments')):

/**
 * TODO: docs
 */
function get_image_attachments($post_id = null, $numberposts = -1, $include_thumbnail = false, $order = 'ASC', $orderby = 'menu_order ID')
{
    global $post;
    if (empty($post_id)) $post_id = $post->ID;

    $params = array(
        'post_parent'    => $post_id,
        'post_type'      => 'attachment',
        'post_mime_type' => 'image',
        'numberposts'    => $numberposts,
        'order'          => $order,
        'orderby'        => $orderby,
    );

    if (!$include_thumbnail)
    {
        $params['exclude'] = array(get_post_thumbnail_id());
    }

    $attachments = get_children($params);
    if ($numberposts == 1 && !empty($attachments))
    {
        $attachments = array_values($attachments);
        return $attachments[0];
    }
    return $attachments;
}

endif;


if (!function_exists(__NAMESPACE__ . '\get_img')):

/**
 * Returns a full path to a WordPress image in the theme's images folder
 *
 * @param string $file  Name of file relative to theme's "images" folder
 *
 * @return string       Path to image relative to stylesheet_directory
 */
function get_img($file)
{
    return get_bloginfo('stylesheet_directory') . "/images/{$file}";
}

endif;


if (!function_exists(__NAMESPACE__ . '\img')):

/**
 * Creates an <img> object and outputs it using WordPress paths
 *
 * @param string $file  Name of file relative to theme's "images" folder
 * @param string $alt   Alt text for image tag
 * @param bool   $echo  Echo to page if true or return content if false
 *
 * @return string|void  An image string or nothing if $echo is true
 */
function img($file, $alt = null, $echo = true)
{
    if (is_null($alt))
    {
        $alt = pathinfo($file, PATHINFO_FILENAME);
        $alt = ucwords(preg_replace("/_|-|\./i", ' ', $alt));
    }
    $alt = esc_attr($alt);
    $file = get_img($file);
    $content = "<img src=\"{$file}\" alt=\"{$alt}\"/>";
    if ($echo)
    {
        echo $content;
    }
    else
    {
        return $content;
    }
}

endif;
