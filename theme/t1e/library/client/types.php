<?php

/**
 * Contains methods for working with custom post types,
 * taxonomies, and custom post meta-data.
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\client;

use \t1e\core\Inflector as Inflector;
use \t1e\core\Registry as Registry;


{
    /**
     * @link \t1e\core\Inflector
     */
    require_once APP_LIBRARY . '/core/inflector.php';

    /**
     * @link \t1e\core\Plugin
     */
    require_once APP_LIBRARY . '/core/plugin.php';

    /**
     * @link \t1e\core\Registry
     */
    require_once APP_LIBRARY . '/core/registry.php';
}


if (!class_exists(__NAMESPACE__ . '\Relationships')):

/**
 * Defines types of post-type-to-post-type relationships
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Relationships
{
    /**
     * Relationship where the post-type is related to one of another post-type
     *
     * @var string
     */
    const HAS_ONE  = 'has_one';

    /**
     * Relationship where the post-type is related to one or more of another post-type
     *
     * @var string
     */
    const HAS_MANY = 'has_many';
}

endif;


if (!class_exists(__NAMESPACE__ . '\Types')):

/**
 * A `\t1e\client\Types` object is assigned to `\t1e\App`’s
 * $types object. This class allows you to quickly and simply
 * register custom post types and custom taxonomies as well
 * as create simple relationships between custom post-types.
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Types extends \t1e\core\Plugin
{
    /**
     * Hook into WordPress filters
     *
     * @var array
     */
    protected $_filters = array(
        'the_content'      => array(9),
        'enter_title_here' => array(10, 2)
    );

    /**
     * Singleton object to stored Registry
     *
     * @var Registry
     */
    private $__registry;

    /**
     * Called before intialization
     *
     * @return void
     */
    protected function _setup()
    {
        parent::_setup();

        $this->__registry = Registry::get_instance();
    }

    /**
     * Remove the wpautop function from filtering the_content() output
     *
     * The default is to filter the content, but certain post
     * types can disable this functionality if you want to use
     * untouched HTML, for example, by setting the <code>remove_autop_filter</code>
     * property to true.
     *
     * @param string $content  Initial unfiltered post content
     *
     * @return string  Potentially filterted post content
     */
    public function the_content($content)
    {
        global $post;

        if (!isset($this->__registry->modules[$post->post_type])) {
            return $content;
        }

        $options = $this->__registry->modules[$post->post_type];
        // do not remove filter by default
        $remove_filter = isset($options['remove_autop_filter']) && (int)$options['remove_autop_filter'];

        if ($remove_filter)
        {
            remove_filter('the_content', 'wpautop');
            remove_filter('the_excerpt', 'wpautop');
        }

        return $content;
    }

    /**
     * Adds a different placeholder to the title field if set in post type options.
     *
     * @param string $title  Default title string
     * @param WP_Post $post  Post instance if editing
     *
     * @return string        New placeholder text
     */
    public function enter_title_here($title, $post)
    {
        if (!isset($this->__registry->modules[$post->post_type])) { return $title; }
        $options = $this->__registry->modules[$post->post_type];
        if (in_array('title', $options['supports']) && isset($options['title_placeholder'])) {
            return __($options['title_placeholder'], APP_LANG);
        }
        return $title;
    }

    /**
     * Helper method for WordPress' register_post_type.
     *
     * `$options` are the same options you'd pass to register_post_type
     * but you can also pass strings for `singular_name` and `plural_name`
     * of the post type in the same array.
     *
     * @param string $post_type    Name of the post type
     * @param string $description  Simple descrition of the post type
     * @param array  $options      Post type options
     *
     * @link http://codex.wordpress.org/Function_Reference/register_post_type
     *
     * @return object|WP_Error
     */
    public function register_post_type($post_type, $description, $options = array())
    {
        $singular_name = ucwords(Inflector::singularize($post_type));
        $plural_name = ucwords(Inflector::pluralize($post_type));

        if (isset($options))
        {
            if (!empty($options['singular_name']))
            {
                $singular_name = $options['singular_name'];
                unset($options['singular_name']);
            }

            if (!empty($options['plural_name']))
            {
                $plural_name = $options['plural_name'];
                unset($options['plural_name']);
            }
        }

        $default_labels = array(
            'name'          => __($plural_name, APP_LANG),
            'singular_name' => __($singular_name, APP_LANG),
            'add_new_item'  => __('Add New ' . $singular_name, APP_LANG),
            'edit_item'     => __('Edit ' . $singular_name, APP_LANG),
            'new_item'      => __('New ' . $singular_name, APP_LANG),
            'view_item'     => __('View ' . $singular_name, APP_LANG),
            'not_found'     => __('No ' . strtolower($plural_name) . ' found.', APP_LANG),
        );

        if (!empty($options['labels']))
        {
            $default_labels = array_merge($default_labels, (array)$options['labels']);
            unset($options['labels']);
        }

        $rewrite = array('slug' => $post_type, 'with_front' => false);
        if (isset($options['rewrite']))
        {
            $rewrite = array_merge($rewrite, (array)$options['rewrite']);
        }

        $supports = array('title', 'editor', 'excerpt', 'thumbnail', 'page-attributes');
        if (!empty($options['exclude_support'])) {
            $supports = array_diff($supports, (array)$options['exclude_support']);
        }

        $defaults = array(
            'labels'         => $default_labels,
            'description'    => $description,
            'public'         => false,
            'has_archive'    => false,
            'hierarchical'   => false,
            'show_ui'        => true,
            'show_in_menu'   => true,
            'menu_position'  => 25,
            'rewrite'        => $rewrite,
            'supports'       => $supports,
        );

        $post_type_options = array_merge($defaults, $options);
        // pr($post_type_options);

        // register post type with WordPress
        $type = register_post_type($post_type, $post_type_options);

        if ($type)
        {
            $this->register_module($post_type, $post_type_options);
        }

        return $type;
    }

    /**
     * Helper method for WordPress' register_taxonomy
     *
     * Generally used alongside `get_taxonomy_label_args`
     *
     * @link get_taxonomy_label_args
     *
     * @return void
     */
    public function register_taxonomy($taxonomy, $post_types, array $args = array())
    {
        $post_types = (array)$post_types;

        if (!isset($args['default_args']))
        {
            $taxonomy_name = Inflector::humanize($taxonomy);
            $plural_name = ucwords(Inflector::pluralize($taxonomy_name));
            $singular_name = ucwords(Inflector::singularize($taxonomy_name));
            $default_args = $this->get_taxonomy_label_args($plural_name, $singular_name);
            $args = array_merge($default_args, $args);
        }

        register_taxonomy($taxonomy, $post_types, $args);
    }

    /**
     * Generate an array of labels for a taxonomy
     *
     * @param string $plural    Plural name of taxonomy
     * @param string $singular  Singular name of taxonomy
     * @param string $slug      Name of rewrite slug
     *
     * @link https://codex.wordpress.org/Function_Reference/register_taxonomy
     *
     * @return array  Labels to supply to taxonomy creator
     */
    public function get_taxonomy_label_args($plural, $singular, $slug = null)
    {
        if (!is_null($slug))
        {
            if (!is_array($slug))
            {
                $slug = array('slug' => $slug);
            }
        }
        return array(
            'default_args' => true,
            'hierarchical' => true,
            'public' => true,
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud' => true,
            'labels' => array(
                'name' => _x($plural, $singular . 'taxonomy general name'),
                'singular_name' => _x($singular, $singular . 'taxonomy singular name'),
                'search_items' =>  __('Search ' . $plural, APP_LANG),
                'popular_items' => __('Popular ' . $plural, APP_LANG),
                'all_items' => __('All ' . $plural, APP_LANG),
                'parent_item' => null,
                'parent_item_colon' => null,
                'edit_item' => __('Edit ' . $singular, APP_LANG),
                'update_item' => __('Update ' . $singular, APP_LANG),
                'add_new_item' => __('Add New ' . $singular, APP_LANG),
                'new_item_name' => __('New ' . $singular . ' Name', APP_LANG),
                'separate_items_with_commas' => __('Separate ' . strtolower($plural) . ' with commas', APP_LANG),
                'add_or_remove_items' => __('Add or remove ' . strtolower($plural), APP_LANG),
                'choose_from_most_used' => __('Choose from the most used ' . strtolower($plural), APP_LANG),
                'menu_name' => __($plural, APP_LANG),
            ),
            'rewrite' => $slug,
        );
    }

    /**
     * Registers a "module"; basically a custom post type with custom fields
     *
     * @param string $moduleName    The module to register
     * @param array $moduleOptions  Options to register the module with
     *
     * @return void
     */
    public function register_module($moduleName, $moduleOptions)
    {
        if ($this->_type_exists($moduleName))
        {
            $moduleOptions = array_merge($this->__registry->modules[$moduleName], $moduleOptions);
        }
        $this->__registry->modules[$moduleName] = $moduleOptions;
    }

    /**
     * Create relationships between two post-types
     *
     * Automatically adds meta-boxes for the relationships on the
     * post-type's edit page.
     *
     * @param string $from         Which post-type to a meta-box to its edit page
     * @param string $to  Which post-type will be displayed in the meta-box
     *                             so on the $from
     * @param string $type         Type of relationship
     *
     * @return void
     */
    public function create_relationship($from, $to, $type = Relationships::HAS_MANY)
    {
        if ($from_module = $this->_type_exists($from))
        {
            if ($to_module = $this->_type_exists($to))
            {
                $relationships = isset($from_module['relationships']) ? $from_module['relationships'] : array();
                $relationships[$to] = $type;
                $this->__registry->modules[$from]['relationships'] = $relationships;
            }
            else
            {
                wp_die(sprintf(__('<strong>Error:</strong> Could not create relationship from <em>%s</em> to <em>%s</em> because <em>%s</em> is not registered as a post type', APP_LANG), $from, $to, $to));
            }
        }
        else
        {
            wp_die(sprintf(__('<strong>Error:</strong> Could not create relationship from <em>%s</em> to <em>%s</em> because <em>%s</em> is not registered as a post type', APP_LANG), $from, $to, $from));
        }
    }

    /**
     * Create "has many" or "has one" relationships between two post-types
     *
     * @param string $from_post_type  Which post-type to a meta-box to its edit page
     * @param string|string[] $to     Which post-type will be displayed in the meta-box
     *                                so on the $from
     * @return void
     */
    public function relate_types($from_post_type, $to)
    {
        $to = (array)$to;
        foreach ($to as $related)
        {
            $to_post_type = $related;
            $relationship = Relationships::HAS_MANY;

            if (is_array($related))
            {
                if (isset($related['post_type'])) $to_post_type = $related['post_type'];
                if (isset($related['relationship'])) $relationship = $related['relationship'];
            }

            $this->create_relationship($from_post_type, $to_post_type, $relationship);
        }
        // pr($this->__registry->modules[$from_post_type]['relationships']);
    }

    /**
     * Check if post type exists
     *
     * @param string $type  Post type to query
     *
     * @return registered post type module as array if it exists
     */
    protected function _type_exists($type)
    {
        return isset($this->__registry->modules[$type]) ? $this->__registry->modules[$type] : false;
    }
}

endif;


namespace t1e;


if (!function_exists(__NAMESPACE__ . '\get_taxonomy')):

/**
 * Get a taxonmony from a custom post type.
 *
 * @return array|WP_Error  List of object terms attached to the post
 */
function get_taxonomy($taxonomy)
{
    global $post;
    return wp_get_object_terms($post->ID, $taxonomy);
}

endif;


if (!function_exists(__NAMESPACE__ . '\get_posts')):

/**
 * Get WordPress posts (generally for fetching custom post types)
 *
 * @param string $post_type  Type of custom post to get
 * @param array $args        Args to pass to WP_Query for posts
 *
 * @return WP_Query          Results of the posts query as a WP_Query instance
 */
function get_posts($post_type, array $args = array())
{
    if (!post_type_exists($post_type))
    {
        throw new \t1e\core\Error(sprintf('The post type called "%s" did not exist', $post_type));
    }

    $defaults = array(
        'post_type'  => $post_type,
        'order'      => 'ASC',
        'orderby'    => 'menu_order',
        'nopaging'   => true,
    );

    $args = array_merge($defaults, $args);
    // pr($args);

    return new \WP_Query($args);
}

endif;


if (!function_exists(__NAMESPACE__ . '\get_posts_by_meta')):

/**
 * Get WordPress posts by meta value
 *
 * @param string $post_type   Type of custom post to get
 * @param string $meta_key    Name of meta key to select on
 * @param mixed  $meta_value  Value of meta key to select on
 * @param array  $args        Args to pass to WP_Query for posts
 *
 * @return WP_Query           Results of the posts query as a WP_Query instance
 */
function get_posts_by_meta($post_type, $meta_key, $meta_value, array $args = array())
{
    $meta_args = array(
        'meta_key'   => $meta_key,
        'meta_value' => $meta_value
    );
    $args = array_merge($meta_args, $args);
    return get_posts($post_type, $args);
}

endif;


if (!function_exists(__NAMESPACE__ . '\get_posts_by_taxonomy')):

/**
 * Get WordPress posts by taxonomy
 *
 * @param string $post_type  Type of custom post to get
 * @param string $taxonomy   Taxonomy slug to select on
 * @param int|string|array   Id, Slug or Array of Ids/Slugs of taxonomy
 * @param array $args        Args to pass to WP_Query for posts
 *
 * @return WP_Query          Results of the posts query as a WP_Query instance
 */
function get_posts_by_taxonomy($post_type, $taxonomy, $taxonomy_ref, array $args = array())
{
    $field = 'slug';
    if (is_array($taxonomy_ref))
    {
        // make some assumptions about selection based on types in array
        // we assume there’s one or more taxonmies in the array
        $field = (is_numeric($taxonomy_ref[0])) ? 'id' : 'slug';
    }
    else if (is_numeric($taxonomy_ref))
    {
        $field = 'id';
    }

    $defaults = array(
        'tax_query' => array(
            array(
                'taxonomy' => $taxonomy,
                'field'    => $field,
                'terms'    => $taxonomy_ref,
            ),
        ),
    );

    $args = array_merge($defaults, $args);

    return get_posts($post_type, $args);
}

endif;


if (!function_exists(__NAMESPACE__ . '\get_sub_pages')):

/**
 * Fetches an array of pages that are children of a given page
 *
 * If `$post_id` is null, uses current global `$post`
 *
 * @param integer $post_id  ID of page to fetch children of
 * @param array   $args     WP_Query args
 *
 * @return WP_Query         Results of the posts query as a WP_Query instance
 */
function get_sub_pages($post_id = null, array $args = array())
{
    global $post;
    if (empty($post_id)) { $post_id = $post->ID; }

    $defaults = array(
        'child_of'    => $post_id,
        'sort_order'  => 'ASC',
        'sort_column' => 'menu_order'
    );
    $args = array_merge($defaults, $args);

    return \get_pages($args);
}

endif;


if (!function_exists(__NAMESPACE__ . '\get_pages')):

/**
 * TODO: docs
 *
 * @global wpdb $wpdb WordPress database abstraction object.
 */
function get_pages_by_name(array $page_names, array $args = array())
{
    global $wpdb;

    $page_names = esc_sql($page_names);
    $page_names = array_map('sanitize_title_for_query', $page_names);
    $in_string = "'" . join("','", $page_names) . "'";

    $sql = "SELECT ID, post_name FROM $wpdb->posts WHERE post_name IN ($in_string) AND post_type = 'page'";
    $results = $wpdb->get_results($sql, OBJECT_K);

    // echo $sql; pr($results);

    $include = array_keys($results);
    $defaults = array(
        'include'     => $include,
        'sort_order'  => 'ASC',
        'sort_column' => 'menu_order'
    );
    $args = array_merge($defaults, $args);

    return \get_pages($args);
}

endif;


if (!function_exists(__NAMESPACE__ . '\get_related_posts')):

/**
 * TODO: implement this
 */
function get_related_posts($post_id = 0)
{
    global $post;
    if (empty($post_id)) { $post_id = $post->ID; }
    echo $post_id;
}

endif;
