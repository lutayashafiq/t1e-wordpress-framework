<?php

/**
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\client;


{
    /**
     * @link t1e\core\Plugin
     */
    require_once APP_LIBRARY . '/core/plugin.php';
}


if (!class_exists(__NAMESPACE__ . '\Sorter')):

/**
 * Base class for custom sort of WordPress' previous_post_link and next_post_link
 *
 * TODO: make this more configurable
 * TODO: docs
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Sorter extends \t1e\core\Plugin
{
    protected $_filters = array(
        'get_previous_post_join' => array(10, 3, 'get_adjacent_post_join'),
        'get_next_post_join' => array(10, 3, 'get_adjacent_post_join'),

        'get_previous_post_where',
        'get_next_post_where',

        'get_previous_post_sort',
        'get_next_post_sort',
    );

    public function get_adjacent_post_join($join, $in_same_term, $excluded_terms)
    {
        return $join;
    }

    public function get_previous_post_where($prepared_query)
    {
        return $this->_build_where_query($prepared_query, 'previous');
    }

    public function get_next_post_where($prepared_query)
    {
        return $this->_build_where_query($prepared_query, 'next');
    }

    public function get_previous_post_sort($the_order_query)
    {
        return $the_order_query;
    }

    public function get_next_post_sort($the_order_query)
    {
        return $the_order_query;
    }

    protected function _build_where_query($prepared_query, $adjacent)
    {
        return $prepared_query;
    }
}

endif;


if (!class_exists(__NAMESPACE__ . '\MenuOrderSorter')):

/**
 * Set up custom sort for WordPress' previous_post_link and next_post_link by menu_order
 *
 * TODO: docs
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class MenuOrderSorter extends Sorter
{
    protected function _build_where_query($prepared_query, $adjacent)
    {
        global $wpdb, $post;

        $comparison = $adjacent === 'previous' ? '<' : '>';
        $prepared_query = $wpdb->prepare("WHERE p.post_type = %s AND p.post_status = 'publish' AND p.menu_order {$comparison} %d", $post->post_type, $post->menu_order);

        return $prepared_query;
    }

    public function get_previous_post_sort($the_order_query)
    {
        $the_order_query = "ORDER BY menu_order DESC LIMIT 1";
        return $the_order_query;
    }

    public function get_next_post_sort($the_order_query)
    {
        $the_order_query = "ORDER BY menu_order ASC LIMIT 1";
        return $the_order_query;
    }
}

endif;
