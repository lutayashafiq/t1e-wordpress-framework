<?php

/**
 * Classes and methods for dealing with content and media
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\client;

{
    /**
     * @link \t1e\core\Plugin
     */
    require_once APP_LIBRARY . '/core/plugin.php';
}


if (!class_exists(__NAMESPACE__ . '\Media')):

/**
 * Adds shortcodes for common video embeds
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Media extends \t1e\core\Plugin
{
    /**
     * Default video embed width (16/9)
     *
     * @var int
     */
    const DEFAULT_WIDTH = 940;

    /**
     * Default video embed height (16/9)
     *
     * @var int
     */
    const DEFAULT_HEIGHT = 529;

    // protected $_shortcodes = array(
    //     'youtube_video',
    //     'vimeo_video',
    //     'video_embed',
    // );

    /////////////////////
    // WordPress Hooks //
    /////////////////////

    // TODO: implement this
    public function youtube_video_shortcode($attrs)
    {
        $content = '';
        if (isset($attrs['src']))
        {
            $id = \t1e\video_id_from_youtube_url($attrs['src']);
            if ($id)
            {
                $url = \t1e\youtube_video_url_with_id($id);
                $content = \t1e\youtube_embed_with_url($url, $attrs['width'], $attrs['height']);
            }
        }

        return $content;
    }

    // TODO: implement this
    public function vimeo_video_shortcode($attrs)
    {
        return \t1e\vimeo_embed('vimeo');
    }

    // TODO: implement this
    public function video_embed_shortcode($attrs)
    {
        return "video";
    }
}

endif;


namespace t1e;

use \t1e\client\Media as Media;


/////////////
// YouTube //
/////////////

if (!function_exists(__NAMESPACE__ . '\video_id_from_youtube_url')):

/**
 * Fetches the ID of a YouTube video given a URL
 *
 * @param string $source  YouTube URL
 *
 * @return mixed          A video ID if matched, otherwise false
 */
function video_id_from_youtube_url($source)
{
    $pattern = '/^(?:(?:(?:https?:)?\/\/)?(?:www.)?(?:youtu(?:be.com|.be))\/(?:watch\?v\=|v\/|embed\/)?([\w\-]+))/is';
    $matches = array();
    preg_match($pattern, $source, $matches);
    if (isset($matches[1])) return $matches[1];
    return false;
}

endif;


if (!function_exists(__NAMESPACE__ . '\youtube_video_url_with_id')):

/**
 * Generate a URL to a YouTube video with a YouTube video ID
 *
 * @param string $id     YouTube video ID
 * @param array $params  Query params to send to video
 *
 * @return string        A YouTube URL
 */
function youtube_video_url_with_id($id, array $params = array())
{
    $query = empty($params) ? '' : '?' . http_build_query($params);
    return esc_url("//youtube.com/embed/{$id}{$query}");
}

endif;


if (!function_exists(__NAMESPACE__ . '\youtube_embed_with_url')):

/**
 * Returns a URL embed code give a URL
 *
 * @param string $url         A YouTube URL
 * @param int|string $width   Width of embed
 * @param int|string $height  Height of embed
 *
 * @return string             Iframe string
 */
function youtube_embed_with_url($url, $width = null, $height = null)
{
    if (is_null($width)) $width = Media::DEFAULT_WIDTH;
    if (is_null($height)) $height = Media::DEFAULT_HEIGHT;
    $url = esc_url($url);
    $width = esc_attr($width);
    $height = esc_attr($height);
    return "<figure class=\"youtube video\"><iframe width=\"{$width}\" height=\"{$height}\" src=\"{$url}\" frameborder=\"0\" allowfullscreen></iframe></figure>";
}

endif;


///////////
// Vimeo //
///////////

if (!function_exists(__NAMESPACE__ . '\video_id_from_vimeo_url')):

/**
 * Fetches the ID of a Vimeo video given a URL
 *
 * @param string $source  Vimeo URL
 *
 * @return mixed          A video ID if matched, otherwise false
 */
function video_id_from_vimeo_url($source)
{
    $pattern = "/^(?:(?:https?:\/\/)?(?:www.)?vimeo.com\/([\w\-]+))/is";
    $matches = array();
    preg_match($pattern, $source, $matches);
    if (isset($matches[1])) return $matches[1];
    return false;
}

endif;


if (!function_exists(__NAMESPACE__ . '\vimeo_video_url_with_id')):

/**
 * Generate a URL to a Vimeo video with a Vimeo video ID
 *
 * @param string $id     Vimeo video ID
 * @param array $params  Query params to send to video
 *
 * @return string        A Vimeo URL
 */
function vimeo_video_url_with_id($id, array $params = array())
{
    $defaults = array(
        'color'    => 'ffffff',
        'title'    => 0,
        'byline'   => 0,
        'portrait' => 0,
    );
    $params = array_merge($defaults, $params);

    $query = empty($params) ? '' : '?' . http_build_query($params);
    return "//player.vimeo.com/video/{$id}{$query}";
}

endif;


if (!function_exists(__NAMESPACE__ . '\vimeo_embed_with_url')):

/**
 * Returns a URL embed code give a URL
 *
 * @param string $url         A Vimeo URL
 * @param int|string $width   Width of embed
 * @param int|string $height  Height of embed
 *
 * @return string             Iframe string
 */
function vimeo_embed_with_url($url, $width = null, $height = null)
{
    if (is_null($width)) $width = Media::DEFAULT_WIDTH;
    if (is_null($height)) $height = Media::DEFAULT_HEIGHT;
    $url = esc_url($url);
    $width = esc_attr($width);
    $height = esc_attr($height);
    return "<figure class=\"vimeo video\"><iframe src=\"{$url}\" width=\"{$width}\" height=\"{$height}\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></figure>";
}

endif;


//////////////////////////
// General video source //
//////////////////////////

if (!function_exists(__NAMESPACE__ . '\video_source_from_url')):

/**
 * Generate a fully qualified embed URL given a video source URL
 *
 * @param string $url    Original source URL
 * @param array $params  URL params to apply to URL
 *
 * @return string        Fully resolved video URL
 */
function video_source_from_url($url, array $params = array())
{
    $id = video_id_from_youtube_url($url);
    if ($id) return youtube_video_url_with_id($id, $params);

    $id = video_id_from_vimeo_url($url);
    if ($id) return vimeo_video_url_with_id($id, $params);

    return $url;
}

endif;


if (!function_exists(__NAMESPACE__ . '\video_embed')):

/**
 * Generates a video embed code given YouTube or Vimeo URL
 *
 * @param string $url    Video URL
 * @param array $params  Params to apply to video URL
 * @param bool $echo     Display content if true, return if false
 *
 * @return mixed         String if $echo is true, void otherwise
 */
function video_embed($url, array $params = array(), $echo = true)
{
    $width = isset($params['width']) ? $params['width'] : Media::DEFAULT_WIDTH;
    $height = isset($params['height']) ? $params['height'] : Media::DEFAULT_HEIGHT;

    $id = video_id_from_youtube_url($url);
    if ($id)
    {
        $url = youtube_video_url_with_id($id, $params);
        $content = youtube_embed_with_url($url, $width, $height);
        if ($echo) echo $content;
        return $content;
    }

    $id = video_id_from_vimeo_url($url);
    if ($id)
    {
        $url = vimeo_video_url_with_id($id, $params);
        $content = vimeo_embed_with_url($url, $width, $height);
    }

    if ($echo) echo $content;
    return $content;
}

endif;
