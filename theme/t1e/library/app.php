<?php

/**
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e;


{
    /**
     * @link \t1e\admin\Core
     */
    require_once APP_LIBRARY . '/admin/core.php';

    /**
     * @link \t1e\client\Core
     */
    require_once APP_LIBRARY . '/client/core.php';

    /**
     * @link \t1e\core\Error
     */
    require_once APP_LIBRARY . '/core/error.php';

    /**
     * @link \t1e\core\Module
     */
    require_once APP_LIBRARY . '/core/module.php';

    /**
     * @link \t1e\core\Plugin
     */
    require_once APP_LIBRARY . '/core/plugin.php';

    /**
     * @link \t1e\core\Inflector
     */
    require_once APP_LIBRARY . '/core/inflector.php';

    /**
     * @link \t1e\core\Registry
     */
    require_once APP_LIBRARY . '/core/registry.php';
}


if (!class_exists(__NAMESPACE__ . '\App')):

/**
 * Base class for WordPress theme application.
 *
 * You can access front-end specific functionality through
 * the $client object which is an instance of `\t1e\client\Core`
 * and back-end specific functionality via the $admin object
 * which is an instance of the `\t1e\admin\Core` class.
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
abstract class App extends core\Plugin
{
    /**
     * Framework version identification
     *
     * @var string
     * @see compareVersion()
     */
    const VERSION = '0.1';

    /**
     * Holds the required PHP version
     *
     * @var string
     */
    const REQUIRED_PHP_VERSION = '5.3.0';

    /**
     * Access to back-end functionality
     *
     * @var \t1e\admin\Core
     */
    public $admin;

    /**
     * Access to front-end functionality
     *
     * @var \t1e\client\Core
     */
    public $client;

    /**
     * Access to registry
     *
     * @var \t1e\core\Registry
     */
    public $registry;

    /**
     * Set up the App by creating the client and admin objects
     *
     * @return void
     */
    protected function _setup()
    {
        $this->registry = core\Registry::get_instance();
        $this->client = new client\Core();

        if (is_admin())
        {
            $this->admin = new admin\Core();
        }
    }

    /**
     * Pass through missing calls to this object to the $client object
     *
     * @return mixed  Result of call to $client object
     */
    public function __call($name, array $arguments)
    {
        return call_user_func_array(array($this->client, $name), $arguments);
    }

    /**
     * Pass through missing get calls to the client object
     *
     * @param string $property  Name of property to fetch
     *
     * @return mixed  Property on the client object
     */
    public function __get($property)
    {
        if (!$this->client->{$property})
        {
            $trace = debug_backtrace();
            trigger_error(
                'Undefined property via __get(): ' . $property .
                ' in ' . $trace[0]['file'] .
                ' on line ' . $trace[0]['line'],
                E_USER_ERROR
            );
        }
        return $this->client->{$property};
    }

    /**
     * Adds a module from the modules directory
     *
     * @param string $moduleName  The module to load
     * @param array $options      Optional option override
     *
     * @return void
     */
    public function add_module($moduleName, array $options = array())
    {
        $modulePath = sprintf('%s/%s.php', APP_MODULES, $moduleName);
        $className = sprintf('\t1e\module\%s', core\Inflector::camelize($moduleName, true, true));

        if (file_exists($modulePath))
        {
            include_once $modulePath;
            if (class_exists($className))
            {
                $moduleClass = new $className($options);
                $this->load_module($moduleClass);
            }
            else
            {
                throw new core\Error(sprintf('Expected to find class "%s" defined in %s', $className, $modulePath));
            }
        }
        else
        {
            throw new core\Error(sprintf('No module file named "%s" found at %s', $moduleName, $modulePath));
        }
    }

    /**
     * Load a module, registering its post type and adding a meta box for it if
     * one is defined by the subclass
     *
     * @param \t1e\core\Module $module  A subclass of the Module class to load
     * @param array $options           Options to override $module’s defaults
     *
     * @return void
     */
    public function load_module(core\Module $module, array $options = array())
    {
        if (!empty($options))
        {
            $module->merge_options($options);
        }

        $this->client->types->register_post_type(
            $module->get_type(),
            $module->get_description(),
            $module->get_options()
        );

        if (!is_null($this->admin))
        {
            $this->admin->meta->prepare_meta_box_for_type($module->get_type(), $module->get_meta_box());
        }

        $this->client->types->register_module($module->get_type(), $module->get_options());

        if ($module->get_is_sortable())
        {
            // TODO: oop somehow?
            orderly_register_orderable_post_type($module->get_type());
        }
    }

    /**
     * Compare the specified framework version string $version
     * with the current VERSION of framework or another version type.
     *
     * @param  string  $version  A version string (e.g. "0.7.1").
     * @return int               -1 if the $version is older,
     *                           0 if they are the same,
     *                           and +1 if $version is newer.
     */
    public static function compareVersion($version, $version_to_compare = self::VERSION)
    {
        $version = strtolower($version);
        $version = preg_replace('/(\d)pr(\d?)/', '$1a$2', $version);
        return version_compare($version, strtolower($version_to_compare));
    }
}

endif;
