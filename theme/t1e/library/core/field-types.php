<?php

/**
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\core;


if (!class_exists(__NAMESPACE__ . '\FieldTypes')):

/**
 * Defines input types that can be added to custom meta boxes in the
 * WordPress admin interface
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class FieldTypes
{
    const TEXT        = 'text';
    const TEXTAREA    = 'textarea';
    const OPTION      = 'option';
    const SELECT      = 'select';
    const COLOR       = 'color';
    const DATE        = 'date';
    const DATEPICKER  = 'datepicker';
    const TIME        = 'time';
    const POST        = 'post';
    const URL         = 'url';
    const EMAIL       = 'email';
    const FILEPICKER  = 'filepicker';
}

endif;
