<?php

/**
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\core;


if (!class_exists(__NAMESPACE__ . '\Plugin')):

/**
 * Base WordPress plugin
 *
 * Quickly register actions and filters without having to
 * do as much manual setup. Most T1E classes extend this
 * class as they share a lot of basic functionality.
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
abstract class Plugin
{
    /**
     * List of WordPress hooks to add at run time
     *
     * In its simplest form, a list of action names you want to
     * add a WordPress action to. For example, array('init') would
     * add a hook to the WordPress 'init' action and call the function
     * 'init' on your subclass:
     *
     *     protected $_actions = array('init');
     *
     *     // called when WordPress runs the 'init' action
     *     public function init() {}
     *
     * Using an associative array allows you to customize the action:
     *
     *     protected $_actions = array('init' => 'my_init');
     *
     * This would hook into the WordPress 'init' action and call the
     * 'my_init' public function on your subclass.
     *
     * Finally, you can set the priority and number of arguments for an action
     * with the following style:
     *
     *     protected $_actions = array('hook_name' => array(10, 3))
     *
     * This method takes an array in the form of: array($priority, $arguments, $method_name)
     *
     * So the above example would call a method on your subclass like:
     *
     *     public function hook_name($a, $b, $c) {}
     *
     * @var array
     */
    protected $_actions = array();

    /**
     * Filters can be added here to hook into WordPress filters where you would
     * normally use `add_filter`. They work in the same way as `$_actions` so
     * review the documentation above for how to use these.
     *
     * @var array
     */
    protected $_filters = array();

    /**
     * Add an array of strings to add shortcodes to your subclass, e.g.:
     *
     *     protected $_shortcodes = array('hello');
     *     public function hello_shortcode() { return 'Hello!'; }
     *
     * A shortcode [hello] is now available in WordPress editor.
     *
     * @var array
     */
    protected $_shortcodes = array();

    /**
     * Setup and initialize the plugin
     *
     * @return void
     */
    public function __construct()
    {
        $this->_setup();
        $this->_init();
    }

    /**
     * Silly test method
     *
     * @return void
     */
    public function hello()
    {
        echo "Hello, world.";
    }

    /**
     * Called before intialization
     *
     * Override to add your functionality to subclass
     *
     * @return void
     */
    protected function _setup()
    {
    }

    /**
     * Initializes the class after setup and adds
     * actions, filters and shortcodes to the plugin
     * based on the protected varibables defined in the
     * subclass. Overridable but make sure to call parent::_init().
     *
     * @return void
     */
    protected function _init()
    {
        if (!empty($this->_actions))
        {
            $this->_add_actions();
        }

        if (!empty($this->_filters))
        {
            $this->_add_filters();
        }

        if (!empty($this->_shortcodes))
        {
            $this->_add_shortcodes();
        }
    }

    /**
     * Add WordPress hooks for actions defined by this object
     *
     * @return void
     */
    protected function _add_actions()
    {
        $this->_add_hooks('action', $this->_actions);
    }

    /**
     * Wrapper for WordPress' <code>add_action</code> method
     *
     * @param string   $action          The name of the action to hook the callback to.
     *                                  The callback is derived from this variable.
     * @param int      $priority        (optional) The order in which the functions associated with a particular action are executed. Lower numbers correspond with earlier execution, and functions with the same priority are executed in the order in which they were added to the action.
     *                                  Default 10.
     * @param int      $accepted_args   (optional) The number of arguments the function accepts.
     *                                  Default 1.
     * @return boolean true             When action is added
     */
    protected function _add_action($action, $priority = 10, $accepted_args = 1)
    {
        return add_action($action, array($this, $action), $priority, $accepted_args);
    }

    /**
     * Add filters set in the $_filters param
     *
     * @return void
     */
    protected function _add_filters()
    {
        $this->_add_hooks('filter', $this->_filters);
    }

    /**
     * Add a WordPress filter hook to the Plugin instance
     *
     *      _add_filter('name')
     *
     * Would add a filter to the 'name' filter and call the
     * `name` function on the Plugin instance.
     *
     * @param string   $filter          The name of the filter to hook the callback to.
     *                                  The callback is derived from this variable.
     * @param int      $priority        (optional) The order in which the functions associated with a particular action are executed. Lower numbers correspond with earlier execution, and functions with the same priority are executed in the order in which they were added to the action.
     *                                  Default 10.
     * @param int      $accepted_args   (optional) The number of arguments the function accepts.
     *                                  Default 1.
     * @return boolean true             When filter is added
     */
    protected function _add_filter($filter, $priority = 10, $accepted_args = 1)
    {
        return add_filter($filter, array($this, $filter), $priority, $accepted_args);
    }

    /**
     * Add a WordPress hook (an action or filter)
     *
     * @param string $hook_type  'filter' or 'action'
     * @param array $hooks       An array of hooks to add
     *
     * @return void
     */
    protected function _add_hooks($hook_type, array $hooks = array())
    {
        // prc($this);
        foreach ($hooks as $var => $val)
        {
            $wp_action = $action = $val;
            $priority = 10;
            $accepted_args = 1;
            if (is_array($val))
            {
                $wp_action = $action = $var;
                // TODO: option to use assoc array
                if (isset($val[0])) $priority = intval($val[0]);
                if (isset($val[1])) $accepted_args = intval($val[1]);
                if (isset($val[2])) $action = strval($val[2]);
            }
            else if (is_string($var))
            {
                $wp_action = $var;
                $action = $val;
            }
            $method = "add_{$hook_type}";
            // pr("$method($wp_action, $action, $priority, $accepted_args)");
            call_user_func($method, $wp_action, array($this, $action), $priority, $accepted_args);
        }
    }

    /**
     * Adds a series of shortcode callbacks to object
     *
     * @return void
     */
    protected function _add_shortcodes()
    {
        foreach ($this->_shortcodes as $code)
        {
            $method = "{$code}_shortcode";
            add_shortcode($code, array($this, $method));
        }
    }
}

endif;
