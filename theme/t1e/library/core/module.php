<?php

/**
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\core;


{
    /**
     * @link \t1e\core\Plugin
     */
    require_once APP_LIBRARY . '/core/plugin.php';
}


if (!class_exists(__NAMESPACE__ . '\Module')):

/**
 * TODO: docs
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Module extends \t1e\core\Plugin
{
    /**
     * Description of the module. Also passed to `register_post_type.`
     *
     * @var string
     */
    protected $_description = 'Module description';

    /**
     * Allow content to be sorted by orderly plugin
     *
     * @var bool
     */
    protected $_is_sortable = false;

    /**
     * Options passed to module and `register_post_type`
     *
     * TODO: docs (all options)
     *
     * @var array
     */
    protected $_options = array();

    /**
     * Custom post type name
     *
     * @var string
     */
    protected $_type;

    /**
     * Allow default options to be overridden
     *
     * @param array $options  Additional options to override `$_options`
     *
     * @return void
     */
    public function __construct(array $options = array())
    {
        $this->merge_options($options);
    }

    /**
     * TODO: docs
     *
     * @param array $options  Additional options to override `$_options`
     *
     * @return \t1e\core\Module
     */
    public function merge_options(array $options)
    {
        $this->_options = array_merge($this->_options, $options);
        return this;
    }

    public function get_meta_box()
    {
        return array();
    }

    public function get_type()
    {
        return $this->_type;
    }

    public function get_description()
    {
        return $this->_description;
    }

    public function get_options()
    {
        return $this->_options;
    }

    public function get_is_sortable()
    {
        return $this->_is_sortable;
    }
}

endif;
