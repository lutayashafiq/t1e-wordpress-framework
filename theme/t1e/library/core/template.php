<?php

/**
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\core;


{
    /**
     * @link t1e\core\Plugin
     */
    require_once APP_LIBRARY . '/core/plugin.php';
}


if (!class_exists(__NAMESPACE__ . '\Template')):

/**
 * Class template for a plugin
 *
 * Copy this file and create a new class based on it
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Template extends \t1e\core\Plugin
{

}

endif;
