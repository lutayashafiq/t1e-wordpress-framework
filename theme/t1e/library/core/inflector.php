<?php

/**
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\core;


if (!class_exists(__NAMESPACE__ . '\Inflector')):

/**
 * The Inflector class handles conversions from plural to singular,
 * class names to table names, and so on. Most of the class is a
 * hack of different methods from different frameworks and custom stuff.
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */
class Inflector
{
    /**
     * List of words that should not be singularlized or pluralized
     *
     * @var array
     */
    private static $__ignores = array(
        "equipment",
        "music",
        "news",
        "staff",
    );

    /**
     * Returns given $lower_case_and_underscored_word as a camelCased word.
     *
     * Hacked version to support - and . as well as _.
     *
     * @param string $lower_case_and_underscored_word  Word to camelize
     * @param bool $capFirst                           LikeThis, not likeThis.
     * @return string                                  Camelized word. likeThis.
     * @link http://cakephp.org
     */
    public static function camelize($lowerCaseAndUnderscoredWord, $capFirst = true, $removeSpaces = false)
    {
        $replace = preg_replace("/_|-|\./i", ' ', $lowerCaseAndUnderscoredWord);
        $replace = ucwords($replace);
        if ($removeSpaces) $replace = str_replace(' ', '', $replace);
        if (!$capFirst) $replace = strtolower(substr($replace, 0, 1)) . substr($replace, 1);
        return $replace;
    }

    /**
     * Attempt to get a foreign key based on table name.
     *
     * Example: using "Articles" for $tableName would return "article_id".
     *
     * Obviously, this is a very basic implementation (not too much overhead...yet).
     *
     * @param string $tableName  Name of table to generate foreign key for
     * @return string            Generated foreign key
     */
    public static function getForeignKey($tableName)
    {
        return strtolower(self::singularize($tableName) . "_id");
    }

    /**
     * Based on a foreign key, attempt to generate a table name.
     *
     * @example Using "article_id" for $foreignKey would return "Articles"
     *
     * @param string $foreignKey  Name of key to generate table for
     * @return string             Generated table
     */
    public static function getForeignKeyTable($foreignKey)
    {
        $key = Typeoneerror_Util_Strings::strstrb($foreignKey, "_id");
        return strtolower(self::pluralize($key));
    }

    /**
     * Get the name of a table based on its model name
     *
     * @param string $model  The model name
     * @return string        The table name
     */
    public static function getTableNameFromModel($model)
    {
        $prefix = "Default_Model_";
        if (strpos($model, $prefix) !== false)
        {
            $model = substr($model, strlen($prefix));
        }
        return self::underscore($model);
    }

    /**
     * Returns a human-readable string from $lower_case_and_underscored_word,
     * by replacing underscores with a space, and by upper-casing the initial characters.
     *
     * @param string $lower_case_and_underscored_word  String to be made more readable
     * @return string                                  Human-readable string
     * @link http://cakephp.org
     */
    public static function humanize($lowerCaseAndUnderscoredWord)
    {
        $replace = ucwords(str_replace("_", " ", $lowerCaseAndUnderscoredWord));
        return $replace;
    }

    /**
     * Simple pluralization of most words.
     *
     * Taken from the CodeIgniter Framework. Added some
     * basic checks for non-inflected words like "news".
     *
     * @param string $str  The word to pluralize
     * @return string      The pluralized word
     * @link http://codeigniter.com/
     */
    public static function pluralize($str, $force = false)
    {
        $str = strtolower(trim($str));
        $end = substr($str, -1);

        $ignores = implode('|', self::$__ignores);
        if (preg_match('/^(' . $ignores . ')$/i', $str))
        {
            return $str;
        }

        if ($end == 'y')
        {
            // Y preceded by vowel => regular plural
            $vowels = array('a', 'e', 'i', 'o', 'u');
            $str = in_array(substr($str, -2, 1), $vowels) ? $str . 's' : substr($str, 0, -1) . 'ies';
        }
        elseif ($end == 's')
        {
            if ($force == true)
            {
                $str .= 'es';
            }
        }
        else
        {
            $str .= 's';
        }

        return $str;
    }

    /**
     * Returns a pretty URL of a camel-cased word.
     *
     * @param string $camel_cased_word  Camel-cased word to be prettied.
     * @return string                   Pretty URL version of the $camelCasedWord
     */
    public static function pretty($camelCasedWord)
    {
        $replace = strtolower(preg_replace('/(?<=\\w)([A-Z])/', '-\\1', $camelCasedWord));
        return $replace;
    }

    /**
     * Creates a more readable version of a camel cased word.
     *
     * For example, if you pass "theModelName", the generated result would
     * be "The Model Name". This is useful for titles and whatnot.
     *
     * @param string $camelCasedWord  The word to split on
     * @return string                 A split on case word with captialized words
     */
    public static function splitOnCaps($camelCasedWord)
    {
        $replace = ucwords(preg_replace("/(?<=\\w)([A-Z])/", " \\1", $camelCasedWord));
        return $replace;
    }

    /**
     * Takes a plural word and makes it singular.
     *
     * Taken from the CodeIgniter Framework. Added support
     * for ignored non-singular such as "news".
     *
     * @param string $str  The word to singularize
     * @return string      The pluralized word
     */
    public static function singularize($str)
    {
        $str = strtolower(trim($str));
        $end = substr($str, -3);

        $ignores = implode("|", self::$__ignores);

        if (preg_match('/^(' . $ignores . ')$/i', $str))
        {
            return $str;
        }

        if ($end == 'ies')
        {
            $str = substr($str, 0, strlen($str) - 3) . 'y';
        }
        elseif ($end == 'ses')
        {
            $str = substr($str, 0, strlen($str) - 2);
        }
        else
        {
            $end = substr($str, -1);

            if ($end == 's')
            {
                $str = substr($str, 0, strlen($str) - 1);
            }
        }

        return $str;
    }

    /**
     * Returns an underscore-syntaxed ($like_this_dear_reader) version of the $camelCasedWord.
     *
     * @param string $camel_cased_word  Camel-cased word to be "underscorized"
     * @return string                   Underscore-syntaxed version of the $camelCasedWord
     * @link http://cakephp.org
     */
    public static function underscore($camelCasedWord)
    {
        $replace = strtolower(preg_replace('/(?<=\\w)([A-Z])/', '_\\1', $camelCasedWord));
        return $replace;
    }
}

endif;
