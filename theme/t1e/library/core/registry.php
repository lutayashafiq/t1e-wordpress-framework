<?php

/**
 * @package    \t1e\core
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


namespace t1e\core;


if (!class_exists(__NAMESPACE__ . '\Registry')):

/**
 * Singleton class for saving data across an app
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 * @final
 */
final class Registry implements \IteratorAggregate
{
    /**
     * Static copy of the class for singleton
     *
     * @var Registry
     */
    private static $_instance;

    /**
     * Private constructor to enforce singleton
     */
    private function __construct()
    {
    }

    /**
     * Get the instance of the Registry
     *
     * @return Registry
     */
    public static function get_instance()
    {
        if (self::$_instance == null)
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * ArrayIterator implementation
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        // The ArrayIterator() class is provided by PHP
        return new \ArrayIterator($this);
    }
}

endif;
