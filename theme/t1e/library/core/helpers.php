<?php

/**
 * General PHP functions for development of themes
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


// TODO: move from global namespace?


if (!function_exists('pr')):

/**
 * Prints out an object to screen for debugging
 *
 * @param mixed $o  The object to print formatted visually
 *
 * @return void
 */
function pr($o)
{
    print '<pre>';
    print_r($o);
    print '</pre>';
}

endif;


if (!function_exists('prc')):

/**
 * Prints name of class and optional message
 *
 * @param class $o         Class to print name of
 * @param string $message  Optional message
 *
 * @return void
 */
function prc($o, $message = '')
{
    pr(get_class($o) . " " . $message);
}

endif;


if (!function_exists('prt')):

/**
 * Prints a list of just the important stuff of WordPress objects
 *
 * @param array $objects  Array of WordPress posts fetched from
 *                        something like `get_posts`
 *
 * @return void
 */
function prt($objects) {
    $func = function($object) {
        return array(
            $object->ID,
            $object->post_title,
            $object->post_name
        );
    };
    pr(array_map($func, $objects));
}

endif;


if (!function_exists('is_assoc')):

/**
 * @return  True if array is associative, false if not
 */
function is_assoc($arr)
{
    return array_keys($arr) !== range(0, count($arr) - 1);
}

endif;


if (!function_exists('redirect')):

/**
 * Redirect via PHP
 *
 * @return void
 */
function redirect($where)
{
    header("location:{$where}");
    exit();
}

endif;


/**
 * Redirect via JavaScript
 *
 * @return void
 */
if (!function_exists('javascript_redirect')):

function javascript_redirect($url)
{
    echo '<script>top.location="' . $url . '";</script>';
    //echo "<a href=\"{$url}\">Go to download location</a>";
    exit();
}

endif;


if (!function_exists('signed')):

/**
 * @return int  A "randomly" signed integer
 */
function signed()
{
    return rand(0, 1) ? 1 : -1;
}

endif;


if (!function_exists('array_keys_mixed')):

/**
 * Get all keys of a mixed array (assoc or numeric indexed)
 *
 * @param array $array  The array to get keys from
 *
 * @return array        An array of mixed keys
 */
function array_keys_mixed($array)
{
    $ret = array();
    foreach ($array as $key => $value)
    {
        if (is_numeric($key)) $ret[] = $value;
        else $ret[] = $key;
    }
    return $ret;
}

endif;


if (!function_exists('t2d')):

/**
 * Convert time into decimal time.
 *
 * @param string $time The time to convert
 *
 * @return integer The time as a decimal value.
 */
function t2d($time)
{
    $timeArr = explode(':', $time);
    $hour = (int)$timeArr[0];
    $minutes = (int)$timeArr[1];
    $decTime = $hour + ($minutes / 60);
    return $decTime;
}

endif;


if (!function_exists('d2t')):

/**
 * Convert decimal time into time in the format hh:mm:ss
 *
 * @param integer The time as a decimal value.
 *
 * @return string $time The converted time value.
 */
function d2t($decimal)
{
    $hours = floor((int)$decimal / 60);
    $minutes = floor((int)$decimal % 60);
    $seconds = $decimal - (int)$decimal;
    $seconds = round($seconds * 60);

    return str_pad($hours, 2, "0", STR_PAD_LEFT) . ":" . str_pad($minutes, 2, "0", STR_PAD_LEFT) . ":" . str_pad($seconds, 2, "0", STR_PAD_LEFT);
}

endif;
