<?php

/**
 * Bootstraps the Typeoneerror WordPress framework.
 *
 * @copyright  Copyright (c) Typeoneerror Studios http://typeoneerror.com
 * @license    MIT (http://opensource.org/licenses/MIT)
 */


// Framework configuration defaults.
// These can be defined in a "config.php" file in the
// theme directory (next to your "functions.php" file).
$defaults = array(
    'APP_NAME'                          => 'appname',
    'APP_LANG'                          => 'appname',
    'APP_VERSION'                       => '1.0.0',
    'APP_SITE_NAME'                     => 'Site Name',
    'APP_TITLE_DIVIDER'                 => ' | ',
    'APP_DESCRIPTION'                   => '',
    'APP_VIEWPORT'                      => 'width=device-width,maximum-scale=1.0,minimum-scale=1.0,initial-scale=1',
    'APP_OG_TYPE'                       => 'website',
    'APP_SCHEMA_TYPE'                   => 'WebPage',
    'APP_OPT_USE_THUMBNAILS'            => true,
    'APP_OPT_DONT_CLEAN_CONTENT'        => false,
    'APP_OPT_ALLOW_RICHEDIT'            => true,
    'APP_OPT_USE_TRANSLATIONS'          => false,
    'APP_OPT_USE_CUSTOM_BACKGROUNDS'    => false,
    'APP_OPT_USE_AUTOMATIC_FEED_LINKS'  => true,
    'APP_OPT_INCLUDE_FOUT_PREVENTION'   => false,
    'APP_OPT_USE_VERSIONING'            => false,
    'APP_OPT_USES_POSTS'                => true,
    'APP_OPT_USES_TAGS'                 => true,
    'APP_OPT_USES_LIBRARY'              => true,
    'APP_OPT_USES_COMMENTS'             => true,
    'APP_OPT_CLIENT_MODE'               => false,
    'APP_OPT_USES_SETTINGS'             => false,
);

foreach ($defaults as $key => $value)
{
    if (!defined($key)) define($key, $value);
}


// Define some useful paths that we can use in the theme
// Prefix all with APP_ to avoid conflict with any current or future WordPress constants
if (!defined('APP_FRAMEWORK'))   define('APP_FRAMEWORK', realpath(dirname(__FILE__)));
if (!defined('APP_THEME_PATH'))  define('APP_THEME_PATH', get_stylesheet_directory());
if (!defined('APP_THEME_URI'))   define('APP_THEME_URI', get_stylesheet_directory_uri());

// Global path definitions
$defines = array(
    'APP_LIBRARY'        => APP_FRAMEWORK . '/library',
    'APP_MODULES'        => APP_FRAMEWORK . '/library/module',
    'APP_PLUGIN_PATH'    => APP_FRAMEWORK . '/plugins',
    'APP_TEMPLATE_PATH'  => APP_THEME_PATH,
    'APP_TEMPLATE_URL'   => APP_THEME_URI,
    'APP_IMAGES_URL'     => APP_THEME_URI . '/images',
    'APP_SCRIPTS_URL'    => APP_THEME_URI . '/scripts',
    'APP_STYLES_URL'     => APP_THEME_URI . '/styles',
    'APP_FRAMEWORK_URL'  => APP_THEME_URI . '/t1e',
    'APP_URI'            => 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']
);

// Set the definitions if not yet defined
foreach ($defines as $key => $value)
{
    if (!defined($key)) define($key, $value);
}


// Include plugins
// TODO: something else!
$plugins = array('orderly');
foreach ($plugins as $plugin)
{
    require_once APP_PLUGIN_PATH . "/{$plugin}/{$plugin}.php";
}


// Import the application class
require_once APP_LIBRARY . '/app.php';
