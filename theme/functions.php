<?php

//////////////////////////////
// Set up the T1E framework //
//////////////////////////////

// Change how the framework works by editing the settings in the config file
require_once('config.php');

// Bootstrap the Typeoneerror framework
require_once('t1e/bootstrap.php');


////////////////////////////////////////
// Add your custom functionality here //
////////////////////////////////////////

use \t1e\core\FieldTypes as FieldTypes;

class Application extends t1e\App
{
    /**
     * Call these methods on this instance when WordPress
     * fires off the hooks of the same name
     *
     * @var array
     */
    protected $_actions = array(
        'init',
        'admin_init',
        'wp_enqueue_scripts',
    );

    /**
     * Initialize the application
     *
     * Here you'll want to register custom post types using
     * some of the helper methods on \t1e\client\Types, e.g.:
     *
     *     $this->types->register_post_type(
     *         'example',
     *         'Description of example',
     *         array(
     *             'menu_icon' => 'dashicons-exerpt-view'
     *         ),
     *     );
     *
     * @return void
     */
    public function init()
    {
    }

    /**
     * Initialize WordPress admin interface
     *
     * Here you'll want to use helpers such as
     *
     *     $this->admin->meta->add_meta_box_for_type('example', array(
     *         'custom_field',
     *         'custom_option' => FieldTypes::OPTION,
     *     ));
     *
     * to add custom meta boxes for your custom post types
     *
     * @return void
     */
    public function admin_init()
    {
    }

    /**
     * Add javascripts to pages
     */
    public function wp_enqueue_scripts()
    {
    }
}

// Starts up the app
$app = new Application();
