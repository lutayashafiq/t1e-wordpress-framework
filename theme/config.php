<?php

////////////
// BASICS //
////////////

// Change the name of the site (this is the base of the domain).
// If your site will launch at "mysitename.com", this could be "mysitename".
define('APP_NAME', 'example');

// Set the language string for the site. The typical method is to set it to the same as APP_NAME.
// NOTE: custom post types have a maximum length, so it may be pertinent to make this value unique, but short.
define('APP_LANG', 'example');

// Appended to styles and scripts as a version number.
define('APP_VERSION', '1.0.0');


//////////////
// METADATA //
//////////////

// Populate og:site_name and apple web app title.
define('APP_SITE_NAME', 'Example');

// Separator for the <title> tag.
define('APP_TITLE_DIVIDER', ' | ');

// General meta data description for Facebook Open Graph and meta tags.
define('APP_DESCRIPTION', '');

// Some archaic keywords that aren't even used any more but some people like
define('APP_KEYWORDS', '');

// Configure the viewport for mobile devices
define('APP_VIEWPORT', 'width=device-width,maximum-scale=1.0,minimum-scale=1.0,initial-scale=1');

// Type of application w/r to Facebook and Schema.org
define('APP_OG_TYPE', 'website');
define('APP_SCHEMA_TYPE', 'WebPage');


//////////////////////
// GOOGLE ANALYTICS //
//////////////////////

// The Google Analytics tracking ID. Leave blank for no Google Analytics.
define('APP_GA_ID', '');

// Base domain for tracking.
define('APP_GA_DOMAIN', 'example.com');


///////////////////////
// WORDPRESS OPTIONS //
///////////////////////

// Use featured/thumbnails on posts and pages if available.
define('APP_OPT_USE_THUMBNAILS', true);

// Stops WordPress from doing auto "pee" and text cleaning when saving a post if true.
define('APP_OPT_DONT_CLEAN_CONTENT', false);

// If true, doesn't hide the rich text box.
define('APP_OPT_ALLOW_RICHEDIT', true);

// Use translations.ini (in theme directory) file to override translations in the WordPress admin.
define('APP_OPT_USE_TRANSLATIONS', true);

// Sets up WordPress admin to allow custom backgrounds.
define('APP_OPT_USE_CUSTOM_BACKGROUNDS', false);

// Sets up front-end to auto generate feed links.
define('APP_OPT_USE_AUTOMATIC_FEED_LINKS', true);

// Adds javascript to delay page display for a bit for browsers that have FOUT problems.
// You may want to enable this if you are using @font-face
define('APP_OPT_INCLUDE_FOUT_PREVENTION', false);

// Disable autosaving and post versions.
// You should also set WP_POST_REVISIONS to false in your wp-config.php if false.
define('APP_OPT_USE_VERSIONING', true);


//////////////////////////
// CONTENT TYPE SUPPORT //
//////////////////////////

// Set to false to hide posts.
define('APP_OPT_USES_POSTS', true);

// Set to false to hide tags.
define('APP_OPT_USES_TAGS', true);

// Set to false to hide comments
define('APP_OPT_USES_COMMENTS', true);

// Set to false to hide the media library.
define('APP_OPT_USES_LIBRARY', true);

// Set to true to hide the submenus Themes, Customize and Editor from the Appearance menu.
define('APP_OPT_CLIENT_MODE', false);

// Create a global settings page. Will look for settings.json file in your theme root.
// Check out the README for how to use.
define('APP_OPT_USES_SETTINGS', true);
