<?php

/**
 * header.php
 *
 * This file gets included at the top of every theme-facing
 * page. You should not really need to edit this, but you can.
 *
 * To change settings in here (like Google Analytics, Descriptions, Facebook
 * open:graph settings, etcetera) edit <theme>/config.php.
 *
 * You should create your actual HTML header in <theme>/top.php
 * which—as you can see below—is included by this file.
 *
 * @author     Benjamin Borowski <ben.borowski@typeoneerror.com>
 * @copyright  Typeoneerror Studios LLC
 * @license    MIT (http://opensource.org/licenses/MIT)
 */

?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<title><?php

global $page, $paged;

// Page and name
$blog_name = get_bloginfo('name');
$full_title = $blog_name;

// Add the blog description for the home/front page.
$site_description = get_bloginfo('description', 'display');
if ($site_description && is_front_page())
{
  $full_title .= APP_TITLE_DIVIDER . $site_description;
}
else if (is_404())
{
  $full_title .= APP_TITLE_DIVIDER . "404";
}
else if (is_home())
{
  $full_title = __('Articles for ' . $blog_name, APP_NAME);
}
else if (is_single())
{
  $full_title = $post->post_title . __(' on ', APP_NAME) . $blog_name;
}
else if (is_page())
{
    $full_title = single_post_title() . APP_TITLE_DIVIDER . $blog_name;
}
else if (!(is_home() || is_front_page()))
{
  $date = APP_TITLE_DIVIDER . get_the_date(get_option('date_format'));
  $full_title .= $date;
}

echo $full_title;

?></title>

<!-- Basic configuration -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta charset="<?php bloginfo("charset"); ?>"/>
<meta http-equiv="imagetoolbar" content="false"/>
<meta name="app-version" content="<?php echo esc_attr(APP_VERSION); ?>"/>

<!-- Meta configuration -->
<?php if (defined('APP_KEYWORDS')): ?>
<meta name="keywords" content="<?php echo esc_attr(APP_KEYWORDS); ?>"/>
<?php endif; ?>
<?php if (defined('APP_VIEWPORT')): ?>
<meta name="viewport" content="<?php echo esc_attr(APP_VIEWPORT); ?>"/>
<?php endif; ?>

<?php  if (!t1e\theme_has_seo_plugin()): ?>
<!-- Facebook Open Graph configuration -->
<meta property="og:title" content="<?php echo esc_attr($full_title); ?>"/>
<meta property="og:site_name" content="<?php echo esc_attr(APP_SITE_NAME); ?>"/>
<meta property="og:type" content="<?php echo esc_attr(APP_OG_TYPE); ?>"/>
<meta property="og:url" content="<?php echo esc_url(APP_URI); ?>"/>
<meta property="og:image" content="<?php echo APP_TEMPLATE_URL; ?>/images/fb-open-graph.png"/>
<?php if (is_front_page()): ?>
<meta property="og:description" content="<?php echo esc_attr(APP_DESCRIPTION); ?>"/>
<?php endif; // is_front_page() ?>
<link rel="image_src" href="<?php echo APP_TEMPLATE_URL; ?>/images/fb-open-graph.png"/>

<!-- Microsoft -->
<meta itemprop="name" content="<?php echo esc_attr($full_title); ?>"/>
<meta itemprop="description" content="<?php echo esc_attr(APP_DESCRIPTION); ?>"/>

<?php endif; // theme_has_seo_plugin() ?>

<!-- Apple -->
<meta name="apple-mobile-web-app-title" content="<?php echo esc_attr(APP_SITE_NAME); ?>"/>
<link rel="apple-touch-icon-precomposed" media="screen and (resolution: 163dpi)" href="<?php echo APP_TEMPLATE_URL; ?>/images/apple-touch-icon.png"/>
<link rel="apple-touch-icon-precomposed" media="screen and (resolution: 326dpi)" href="<?php echo APP_TEMPLATE_URL; ?>/images/apple-touch-icon-retina.png"/>
<link rel="apple-touch-icon-precomposed" media="screen and (resolution: 132dpi)" href="<?php echo APP_TEMPLATE_URL; ?>/images/apple-touch-icon-ipad.png"/>
<link rel="apple-touch-icon-precomposed" media="screen and (resolution: 264dpi)" href="<?php echo APP_TEMPLATE_URL; ?>/images/apple-touch-icon-ipad-retina.png"/>

<!-- Other fun stuff -->
<link rel="shortcut icon" href="<?php echo APP_TEMPLATE_URL; ?>/images/favicon.ico" />
<link rel="profile" href="http://gmpg.org/xfn/11"/>

<?php wp_head(); ?>

<?php
$ga = defined('APP_GA_ID') ? APP_GA_ID : null;
if (!empty($ga)):
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php echo APP_GA_ID; ?>', '<?php echo APP_GA_DOMAIN; ?>');
  ga('send', 'pageview');
</script>
<?php
endif;
?>

<?php if (APP_OPT_INCLUDE_FOUT_PREVENTION): ?>
<script type="text/javascript">
//<![CDATA[
(function(){
    // hide content till load (or 3 seconds) to prevent FOUT in certain browsers
    var d = document, e = d.documentElement, s = d.createElement('style');
    if (e.style.MozTransform === ''){ // gecko 1.9.1 inference
    s.textContent = 'body{visibility:hidden}';
    e.firstChild.appendChild(s);
    function f(){ s.parentNode && s.parentNode.removeChild(s); }
        addEventListener('load',f,false);
        setTimeout(f, 3000);
    }
})();
//]]>
</script>
<?php endif; ?>

<?php

include(locate_template('top.php'));

?>
