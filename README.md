T1E WordPress Framework
=======================

> NOTE: this is very much a work-in-progress. Please direct any questions to [hello@typeoneerror.com](mailto:hello@typeoneerror.com).


Overview
--------

The T1E (aka Typeoneerror) WordPress Framework is a (mostly) object-oriented framework built on top of WordPress. It's essentially a theme framework focused on making custom CMSes with WordPress much simpler and less wordy.

NOTE: The _theme_ folder is a very barebones theme. The expectation here is that you are creating a theme from scratch and you'll be utilizing the framework to do so. An index.php page and a page.php template are all that are provided.


Using the Framework
-------------------

The framework itself lives in _theme/t1e_. To use it:

1. Copy _theme_ to *wp-content/themes/THEME_NAME* where *THEME_NAME* is the name of your own theme. 
2. Edit _wp-content/themes/THEME_NAME/config.php_ to set up your site (see documentation below for configuration options).
3. Log into WordPress Admin and activate the theme under _Appearance > Themes_.
4. Add functionality to _wp-content/themes/theme_name/functions.php_.
5. Hack away!


Configuration
-------------

Edit the constants in the _wp-content/themes/THEME_NAME/config.php_ file to change the 
setup of the framework.

### Basics

#### APP_NAME `String`
Change the name of the site (this is the base of the domain). For example, if your site will launch at "mysitename.com", this could be "mysitename"
    
#### APP_LANG `String`
Set the language string for the site. Easiest is to set it to the same as APP_NAME. NOTE: custom post types have a maximum length, so it may be pertinent to make this value unique, but short.

#### APP_VERSION `String`
Appended to styles and scripts as a version number. You should change this when your assets or site is updated. Perhaps even with a build system such as [Grunt](http://gruntjs.com/) or [Gulp](http://gulpjs.com/).

### Metadata

#### APP_SITE_NAME `String`
Populates `og:site_name` and apple web app titles if you are not using an [SEO plugin](https://yoast.com/wordpress/plugins/seo/).

#### APP_TITLE_DIVIDER `String` (default: ‘ | ’)
Separator for the &lt;title&gt; tag.

#### APP_DESCRIPTION `String`
General meta data description for Facebook Open Graph and meta tags.

#### APP_KEYWORDS `String`
Some archaic keywords that aren't even used any more but some people like.

#### APP_VIEWPORT `String` (default: ‘width=device-width,maximum-scale=1.0,minimum-scale=1.0,initial-scale=1’)
Configure the viewport for mobile devices

#### APP_OG_TYPE `String` (default: ‘website’)
#### APP_SCHEMA_TYPE `String` (default: ‘WebPage’)
Type of application w/r to Facebook and Schema.org, respectively

### Google Analytics

#### APP_GA_ID `String` (default: ‘’)
The Google Analytics tracking id. If this is empty, Google Analytics is not added.

#### APP_GA_DOMAIN `String`
Base domain for tracking.

### WordPress Options

#### APP_OPT_USE_THUMBNAILS `Boolean` (default: true)
Use featured/thumbnails on posts and pages if available.

#### APP_OPT_DONT_CLEAN_CONTENT `Boolean` (default: false)
Stops WordPress from doing auto "pee" and text cleaning when saving a post.

#### APP_OPT_ALLOW_RICHEDIT `Boolean` (default: true)
If true, doesn't hide the rich text box.

#### APP_OPT_USE_TRANSLATIONS `Boolean` (default: true)
Use *translations.ini* (in theme directory) file to override translations in the WordPress admin.

#### APP_OPT_USE_CUSTOM_BACKGROUNDS `Boolean` (default: false)
Sets up WordPress admin to allow custom backgrounds.

#### APP_OPT_USE_AUTOMATIC_FEED_LINKS `Boolean` (default: true)
Sets up front-end to auto generate feed links.

#### APP_OPT_INCLUDE_FOUT_PREVENTION `Boolean` (default: false)
Adds javascript to delay page display for a bit for browsers that have [FOUT](http://www.paulirish.com/2009/fighting-the-font-face-fout/) problems. You may want to enable this if you are using `@font-face`.

#### APP_OPT_USE_VERSIONING `Boolean` (default: true)
Disable autosaving and post versions. You should also set `WP_POST_REVISIONS` to false in your *wp-config.php* if false.

### Content Type Support

#### APP_OPT_USES_POSTS `Boolean` (default: true)
Set to false to hide posts.

#### APP_OPT_USES_TAGS `Boolean` (default: true)
Set to false to hide tags.

#### APP_OPT_USES_COMMENTS `Boolean` (default: true)
Set to false to hide comments.

#### APP_OPT_USES_LIBRARY `Boolean` (default: true)
Set to false to hide the media library.

#### APP_OPT_CLIENT_MODE `Boolean` (default: false)
Set to true to hide the submenus Themes, Customize and Editor from the Appearance menu.

#### APP_OPT_USES_SETTINGS `Boolean` (default: true)
Create a global settings page. Will look for *settings.json* file in your theme root. Check out the README for JSON syntax for creating settings items.


Documentation
-------------

TODO


Settings 
--------

TODO

Translations
------------

TODO
